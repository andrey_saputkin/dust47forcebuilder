Dust Warfare Force Builder
(C) 2012-2014 Chris Jacobson

Dust Warfare Force Builder is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/)

This means you may fork for private use and make changes, but you are not allowed to share your changes except to submit them back via pull request to the main repository (https://bitbucket.org/mainecoon/dustforcebuilder).

Additionally, you may not host a derivative of the Dust Warfare Force Builder tool for public access.  This is to protect the Dust Warfare community and maintain a single version for use.

# TODOs / Known Issues - short term:

# TODOs / Known Issues - long term:
 * Add the Dust 48 units (http://www.dustgame.com/downloadfile.php).
