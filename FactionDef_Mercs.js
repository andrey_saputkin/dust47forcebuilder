// Mercenaries

//Mercenary platoon upgrades

//Mercenary Unit upgrades

//Mercenary Units
//Mercenary Infantry Units
var sqMercCleaningTeam = new UnitDefinition('MclT', '"Sisters of Demolition" Mercenary Cleaning Team', 20);

//Mercenary Vehicles
var vhMercISU203SergeiShtemenko = new UnitDefinition('mISU203', 'ISU-203 "Sergei Shtemenko"', 105, UnitType.Vehicle);
var vhMercK47KAleksei        = new UnitDefinition('mKV47K', 'KV47-K (TSH) "Aleksei"', 35, UnitType.Vehicle);
var vhMercLPWIDHermannTrop  = new UnitDefinition('mLPW1D',  'LPW I-D "Hermann (Trop)"', 24, UnitType.Vehicle);
var vhMercTPWVIGStummel     = new UnitDefinition('mTPW6G',  'TPW VI-G "Stummel"', 50, UnitType.Vehicle);
var vhMercM2F2SteelRainLight   = new UnitDefinition('mM2F2',  'MCW M2-F2 "Steel Rain (Light)"', 39, UnitType.Vehicle);
var vhMercM2GBarkingDogLight   = new UnitDefinition('mM2GL',  'MCW M2-G "Barking Dog (Light)"', 45, UnitType.Vehicle);

var vhMercAnyVehicles = [ vhMercISU203SergeiShtemenko, vhMercK47KAleksei, vhMercLPWIDHermannTrop, vhMercTPWVIGStummel, vhMercM2F2SteelRainLight, vhMercM2GBarkingDogLight ];

//Mercenary Heroes
var hRedYana_Bondarenko		= new UnitDefinition("RdYnB", 'Red Yana (Bondarenko)', 22, UnitType.Hero);

var hMercAnyHeroes = [ hRedYana_Bondarenko ];

//Default Sections
var anyMerc_2ndSectionUnits = [  ]; 
var anyMerc_3rdSectionUnits = [  ]; 
var anyMerc_4thSectionUnits = [  ]; 
var anyMerc_SupportSectionUnits = [ sqMercCleaningTeam ];

//Mercenary Cleaning Platoon Definition
var mercCleaningPlatoon = new PlatoonDefinition('MCle', 'Mercenary Cleaning Platoon');
mercCleaningPlatoon.sections = [
    [ hRedYana_Bondarenko ],
    [ sqMercCleaningTeam],
    [ null, sqMercCleaningTeam ],
    [ null, sqMercCleaningTeam ],
    [ null, sqMercCleaningTeam ]
];
mercCleaningPlatoon.supports = [
	vhMercISU203SergeiShtemenko, vhMercK47KAleksei, 
	vhMercLPWIDHermannTrop, vhMercTPWVIGStummel,
	vhMercM2F2SteelRainLight, vhMercM2GBarkingDogLight
];
mercCleaningPlatoon.additionalSupports = {
    'Support': [ null, vhMercISU203SergeiShtemenko ]
};

/*
//Mercenary Faction Definition
var mercFaction = new FactionDefinition(Faction.Merc, 'Mercenary');
mercFaction.upgrades = [ null ];
mercFaction.unitUpgrades = [  ];
mercFaction.platoons = [ mercCleaningPlatoon ];
mercFaction.heroes = [ hRedYana_Bondarenko ];
*/
