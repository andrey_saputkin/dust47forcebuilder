//Ideas and TODOS:
// * Might be easier maintain the list of capturable vehicles by defining capturability per vehicle unit, and trim the list at runtime.

var Faction = {
    Allies: 0,
    Axis: 1,
    SSU: 2,
};

var UnitType = {
    Soldier: 0,
    Vehicle: 1,
    Hero: 2,
    Solo: 3
};

var SectionName = [ "Command", "1st", "2nd", "3rd", "4th" ];
var NumSections = 5;

var capturedExtraPointsCost = 5;

var Units = {};

function UnitDefinition(id, name, points, type)
{
    this.id = id;
    this.name = name;
    this.points = points;
    this.type = type || UnitType.Soldier;
    this.supports = [];
    this.numCommissars = 0;
    this.mutualExclusive = [];
    this.upgrades = [];
    this.upgradesAllowed = true;

    if (this.id in Units)
    {
        console.error('Unit id ' + id + ' ("' + name + '") already defined');
    }
    Units[this.id] = this;
}


function PlatoonDefinition(id, name)
{
    this.id = id;
    this.name = name;

    this.sections = [
        [], // Command
        [], // 1st
        [], // 2nd
        [], // 3rd
        []  // 4th
    ];
    this.supports = [];
    this.additionalSupports = {};
    this.numCommissars = 0;
    this.commissarsAllowed = true;
    this.upgradesAllowed = true;

    this.prevalidate = function(/*platoon*/) { return true; };
    this.validate = function(/*platoon*/) { return true; };
    this.limitedUpgrades = [];
}


var Upgrades = {};

function UpgradeDefinition(id, name, points)
{
    this.id = id;
    this.name = name;
    this.points = points;
    this.supports = [];
    this.bonusHero = false;
    this.numCommissars = 0;
    this.prevalidate = function(/*platoon*/) { return true; };
    this.validate = function(/*platoon*/) { return true; };
    this.computeSupports = function(/*platoon*/) { return this.supports; };

    if (this.id in Upgrades)
    {
        console.error('Upgrade id ' + id + ' ("' + name + '") already defined');
    }
    Upgrades[this.id] = this;
}


function FactionDefinition(faction, name)
{
    this.id = faction;
    this.name = name;
    this.upgrades = [];
    //this.unitUpgrades = [];
    this.platoons = [];
    this.heroes = [];
    this.capturableVehicles = [];
}

