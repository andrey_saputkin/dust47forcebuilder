/**
 * Code is Copyright 2012 Chris Jacobson
 */

var Faction = {
    Allies: 0,
    Axis: 1,
    SSU: 2
};

var NumSections = 5;

function Squad(id)
{
    this.id = id;
    this.supports = [];
}


function PlatoonDefinition(id)
{
    this.id = id;

    this.sections = [
        [], // Command
        [], // 1st
        [], // 2nd
        [], // 3rd
        []  // 4th
    ];
    this.supports = [];
    this.upgradesAllowed = true;
}


function UpgradeDefinition(id)
{
    this.id = id;
    this.supports = [];
    this.computeSupports = function(platoondefinition) { return this.supports; }
}


function FactionDefinition(faction)
{
    this.faction = faction;
    this.upgrades = [];
    this.platoons = [];
    this.heroes = [];
}


//  ALLIES
//  - Commands
var sqRangerCommand = new Squad('RCom');
var sqHeavyRangerCommand = new Squad('HRCm');
var sqRangerAttack = new Squad('RAtk');

var sqAssaultRanger = new Squad('AssR');
var sqCombatRanger = new Squad('CmbR', 17);
var sqCombatRangerUpg = new Squad('CmR+');
var sqRangerWeapon = new Squad('RnWp');
var sqReconRanger = new Squad('RcnR');

var sqRangerSniperTeam = new Squad('RSnp');
var sqRangerObserverTeam = new Squad('RObs');

var sqHeavyRangerAssault = new Squad('HRAs');
var sqHeavyRangerAttack = new Squad('HRAt');
var sqHeavyRangerTankHunter = new Squad('HRTH');
var sqBritishParatroops = new Squad('BPra');

var sqBritishCommandosKillSquad = new Squad('BCKS');
var sqFrenchForeignLegionKillSquad = new Squad('FFLK');

var vhM1AWildfire = new Squad('M1A');
var vhM1BBlackhawk = new Squad('M1B');
var vhM1CHoney = new Squad('M1C');

var vhM2AMickey = new Squad('M2A');
var vhM2BHotDog = new Squad('M2B');
var vhM2CPounder = new Squad('M2C');
var vhM2FSteelRain = new Squad('M2F');

var vhM3DCobra = new Squad('M3D');
var vhM3DCobraUpg = new Squad('M3D+');
var vhM3ERattler = new Squad('M3E');
var vhM3ERattlerUpg = new Squad('M3E+');

var vhM6APunisher = new Squad('M6A');
var vhM6BFireball = new Squad('M6B');

var hBazookaJoe = new Squad('BJoe');
var hRhino = new Squad('Rhno');
var hActionJackson = new Squad('AcJk');
var hRosie = new Squad('Rosi');
var hOzz117 = new Squad('O117');
var hThePriest = new Squad('Prst');
var hJohnnyOneEye = new Squad('JnyI');
var hTheChef = new Squad('Chef');

var upgAddtlResources = new UpgradeDefinition('AdRs');
var upgAirDrop = new UpgradeDefinition('ArDp');
var upgAlliesImprovedCommand = new UpgradeDefinition('AICm');
var upgAeroncaStrike = new UpgradeDefinition('GB9S');
var upgExtraHero = new UpgradeDefinition('XHro', 5);
var upgPreparatoryBarrage = new UpgradeDefinition('PrpB');


sqRangerCommand.supports = [ null, sqRangerObserverTeam, sqRangerSniperTeam, sqFrenchForeignLegionKillSquad ];
sqHeavyRangerCommand.supports = [ null, sqRangerObserverTeam, sqRangerSniperTeam, sqFrenchForeignLegionKillSquad ];
sqRangerAttack.supports = [ null, sqRangerObserverTeam, sqRangerSniperTeam, sqFrenchForeignLegionKillSquad ];

var alliesCombatPlatoon = new PlatoonDefinition('ACmb');
alliesCombatPlatoon.sections = [
    [ sqRangerCommand, hBazookaJoe ],
    [ sqCombatRanger, sqCombatRangerUpg, sqAssaultRanger, sqRangerWeapon, sqHeavyRangerAttack ],
    [ null, sqCombatRanger, sqCombatRangerUpg, sqAssaultRanger, sqReconRanger, sqHeavyRangerAssault, sqFrenchForeignLegionKillSquad ],
    [ null, sqCombatRanger, sqCombatRangerUpg, sqRangerWeapon, sqHeavyRangerTankHunter ],
    [ null, sqAssaultRanger, sqReconRanger, sqRangerWeapon, sqBritishParatroops, sqBritishCommandosKillSquad ]
];
alliesCombatPlatoon.supports = [
    sqRangerObserverTeam, sqRangerSniperTeam,
    vhM1AWildfire, vhM1BBlackhawk, vhM1CHoney,
    vhM2AMickey, vhM2BHotDog, vhM2CPounder, vhM2FSteelRain,
    vhM6APunisher, vhM6BFireball,
    vhM3DCobra, vhM3DCobraUpg, vhM3ERattler, vhM3ERattlerUpg
];


var alliesElitePlatoon = new PlatoonDefinition('AElt');
alliesElitePlatoon.sections = [
    [ sqHeavyRangerCommand, hRhino, hOzz117 ],
    [ sqHeavyRangerAssault, sqHeavyRangerAttack, sqHeavyRangerTankHunter, sqBritishParatroops, sqBritishCommandosKillSquad, sqFrenchForeignLegionKillSquad ],
    [ null, sqHeavyRangerAttack, sqHeavyRangerTankHunter, sqReconRanger, sqBritishParatroops, sqBritishCommandosKillSquad ],
    [ null, sqHeavyRangerAttack, sqCombatRanger, sqCombatRangerUpg, sqRangerWeapon ],
    [ null, sqHeavyRangerTankHunter, sqCombatRanger, sqCombatRangerUpg, sqRangerWeapon, sqReconRanger ]
];
alliesElitePlatoon.supports = [
    sqRangerObserverTeam, sqRangerSniperTeam,
    vhM1AWildfire, vhM1BBlackhawk, vhM1CHoney,
    vhM2AMickey, vhM2BHotDog, vhM2CPounder
];


var alliesAssaultPlatoon = new PlatoonDefinition('AAsl');
alliesAssaultPlatoon.sections = [
    [ sqRangerAttack, hJohnnyOneEye ],
    [ sqAssaultRanger, sqReconRanger, sqHeavyRangerAssault, sqFrenchForeignLegionKillSquad ],
    [ null, sqAssaultRanger, sqReconRanger, sqHeavyRangerAttack ],
    [ null, sqCombatRanger, sqCombatRangerUpg, sqAssaultRanger, sqBritishParatroops, sqBritishCommandosKillSquad ],
    [ null, sqReconRanger, sqRangerWeapon, sqHeavyRangerTankHunter, sqRangerObserverTeam, sqRangerSniperTeam ]
];
alliesAssaultPlatoon.supports = [
    sqRangerObserverTeam, sqRangerSniperTeam,
    vhM1AWildfire, vhM1BBlackhawk, vhM1CHoney,
    vhM6APunisher, vhM6BFireball
];

var alliesParatroopPlatoon = new PlatoonDefinition('AJks');
alliesParatroopPlatoon.sections = [
    [ hActionJackson ],
    [ sqBritishParatroops, sqBritishCommandosKillSquad ],
    [ null, sqBritishParatroops, sqBritishCommandosKillSquad ],
    [ null, sqBritishParatroops, sqBritishCommandosKillSquad ],
    [ null, sqBritishParatroops, sqBritishCommandosKillSquad ]
];
alliesParatroopPlatoon.upgradesAllowed = false;


var alliesFaction = new FactionDefinition(Faction.Allies);
alliesFaction.upgrades = [ null, upgAddtlResources, upgAirDrop, upgAlliesImprovedCommand, upgAeroncaStrike, upgExtraHero, upgPreparatoryBarrage ];
alliesFaction.platoons = [ alliesCombatPlatoon, alliesElitePlatoon, alliesAssaultPlatoon, alliesParatroopPlatoon ];
alliesFaction.heroes = [ hBazookaJoe, hRhino, hActionJackson, hRosie, hOzz117, hThePriest, hJohnnyOneEye, hTheChef];


//  AXIS
var sqKommandotrupp = new Squad('Ktrp');
var sqHeavyKommandotrupp = new Squad('HKtp');
var sqSturmpioniere = new Squad('Strm')

var sqBattleGrenadiers = new Squad('BGrn');
var sqBattleGrenadiersUpg = new Squad('BGr+');
var sqLaserGrenadiers = new Squad('LGrn');
var sqReconGrenadiers = new Squad('RGrn');
var sqAxisZombies = new Squad('Zmbs');

var sqSniperGrenadierTeam = new Squad('SnpG');
var sqBeobachterTeam = new Squad('Beob');

var sqAxisGorillas = new Squad('Grla');
var sqHeavyReconGrenadiers = new Squad('HRGr');
var sqHeavyLaserGrenadiers = new Squad('HLGr');
var sqHeavyFlakGrenadiers = new Squad('HFGr');

var sqJagdgrenadiere = new Squad('Jgdg');
var sqLaserJagdgrenadiere = new Squad('LJgd');

var vhLPWIAHeinrich = new Squad('LPW1A');
var vhLPWIBHermann = new Squad('LPW1B');
var vhLPWICHans = new Squad('LPW1C');

var vhMPWIIALuther = new Squad('MPW2A');
var vhMPWIIBLudwig = new Squad('MPW2B');
var vhMPWIIDLothar = new Squad('MPW2D');
var vhMPWIIELoth = new Squad('MPW2E');

var vhMPWIIIAWotan = new Squad('MPW3A');
var vhMPWIIIAWotanUpg = new Squad('MPW3A');
var vhMPWIIIDFlammLuther = new Squad('MPW3D');

var vhHPWVIAKonigsluther = new Squad('HPW6A');
var vhHPWVIBSturmkonig = new Squad('HPW6B');
var vhTPWVICPrinzluther = new Squad('TPW6C');
var vhTPWVIDSturmprinz = new Squad('TPW6D');

var hSigridVonThaler = new Squad('SgVT');
var hLara = new Squad('Lara');
var hGrenadierX = new Squad('GrnX');
var hMarkus = new Squad('Mrks');
var hManfred = new Squad('Mnfd');
var hAngela = new Squad('Angl');
var hStefan = new Squad('Stfn');
var hTotenmeister = new Squad('Totn');

var upgDefenses = new UpgradeDefinition('Dfns');
var upgExtraPanzerSupport = new UpgradeDefinition('XPnz');
upgExtraPanzerSupport.supports = [ vhLPWIAHeinrich, vhLPWIBHermann, vhLPWICHans, vhMPWIIALuther, vhMPWIIBLudwig, vhMPWIIDLothar, vhMPWIIELoth, vhMPWIIIAWotan, vhMPWIIIAWotanUpg, vhMPWIIIDFlammLuther ];
upgExtraPanzerSupport.computeSupports = function (platoondefinition) {
    var supports = [];
    for (var s in this.supports)
    {
        var support = this.supports[s];
        if ($.inArray(support, platoondefinition.supports) >= 0)
        {
            supports.push(support);
        }
    }
    return supports;
}
var upgImplacable = new UpgradeDefinition('Impl');
var upgAxisImprovedCommand = new UpgradeDefinition('XICm');
var upgLightningWar = new UpgradeDefinition('LtnW');
var upgNebelwerferBarrage = new UpgradeDefinition('NBar');

sqKommandotrupp.supports = [ null, sqBeobachterTeam, sqSniperGrenadierTeam, sqJagdgrenadiere, vhTPWVICPrinzluther ];
sqHeavyKommandotrupp.supports = [ null, sqBeobachterTeam, sqSniperGrenadierTeam, sqJagdgrenadiere, vhTPWVICPrinzluther ];
sqSturmpioniere.supports = [ null, sqBeobachterTeam, sqSniperGrenadierTeam, sqJagdgrenadiere, vhTPWVICPrinzluther ];

var axisSturmgrenadierePlatoon = new PlatoonDefinition('XStm');
axisSturmgrenadierePlatoon.sections = [
    [ sqKommandotrupp, hManfred, hStefan ],
    [ sqBattleGrenadiers, sqBattleGrenadiersUpg, sqLaserGrenadiers, sqReconGrenadiers, sqHeavyFlakGrenadiers ],
    [ null, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqLaserGrenadiers, sqAxisGorillas ],
    [ null, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqReconGrenadiers, sqHeavyReconGrenadiers, sqHeavyLaserGrenadiers, sqLaserJagdgrenadiere ],
    [ null, sqLaserGrenadiers, sqReconGrenadiers, sqHeavyReconGrenadiers, sqHeavyLaserGrenadiers, sqLaserJagdgrenadiere ]
];
axisSturmgrenadierePlatoon.supports = [
    sqBeobachterTeam, sqSniperGrenadierTeam,
    vhLPWIAHeinrich, vhLPWIBHermann, vhLPWICHans,
    vhMPWIIALuther, vhMPWIIBLudwig, vhMPWIIDLothar, vhMPWIIELoth,
    vhHPWVIAKonigsluther, vhHPWVIBSturmkonig,
    vhMPWIIIAWotan, vhMPWIIIAWotanUpg, vhMPWIIIDFlammLuther,
    sqJagdgrenadiere,
    vhTPWVICPrinzluther, vhTPWVIDSturmprinz
];

var axisSchwerPlatoon = new PlatoonDefinition('XSch');
axisSchwerPlatoon.sections = [
    [ sqHeavyKommandotrupp, hLara ],
    [ sqAxisGorillas, sqHeavyReconGrenadiers, sqHeavyFlakGrenadiers, sqHeavyLaserGrenadiers, sqLaserJagdgrenadiere ],
    [ null, sqHeavyReconGrenadiers, sqHeavyFlakGrenadiers, sqHeavyLaserGrenadiers, sqLaserJagdgrenadiere ],
    [ null, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqReconGrenadiers, sqHeavyReconGrenadiers ],
    [ null, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqReconGrenadiers, sqHeavyReconGrenadiers, sqHeavyLaserGrenadiers, sqLaserJagdgrenadiere ]
];
axisSchwerPlatoon.supports = [
    sqBeobachterTeam, sqSniperGrenadierTeam,
    vhMPWIIALuther, vhMPWIIBLudwig, vhMPWIIELoth,
    vhHPWVIAKonigsluther, vhHPWVIBSturmkonig,
    sqJagdgrenadiere
];

var axisBlutkreuzPlatoon = new PlatoonDefinition('XBlt');
axisBlutkreuzPlatoon.sections = [
    [ sqSturmpioniere, hSigridVonThaler, hManfred ],
    [ sqAxisZombies, sqHeavyLaserGrenadiers, sqLaserGrenadiers, sqLaserJagdgrenadiere],
    [ null, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqHeavyFlakGrenadiers, sqLaserGrenadiers ],
    [ null, sqAxisGorillas, sqAxisZombies, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqHeavyReconGrenadiers ],
    [ null, sqAxisGorillas, sqAxisZombies, sqBattleGrenadiers, sqBattleGrenadiersUpg, sqReconGrenadiers, sqLaserGrenadiers ]
];
axisBlutkreuzPlatoon.supports = [
    sqBeobachterTeam, sqSniperGrenadierTeam,
    vhLPWIAHeinrich, vhLPWIBHermann, vhLPWICHans,
    vhMPWIIALuther, vhMPWIIBLudwig,
    vhHPWVIAKonigsluther, vhHPWVIBSturmkonig,
    vhMPWIIIAWotan, vhMPWIIIAWotanUpg,
    sqJagdgrenadiere,
    vhTPWVICPrinzluther, vhTPWVIDSturmprinz
];


var axisGorillaPlatoon = new PlatoonDefinition('Mrks');
axisGorillaPlatoon.sections = [
    [ hMarkus ],
    [ sqAxisGorillas ],
    [ null, sqAxisGorillas ],
    [ null, sqAxisGorillas ],
    [ null, sqAxisGorillas ]
];
axisGorillaPlatoon.upgradesAllowed = false;


var axisZombiePlatoon = new PlatoonDefinition('Ttnm');
axisZombiePlatoon.sections = [
    [ hTotenmeister ],
    [ sqAxisZombies ],
    [ null, sqAxisZombies ],
    [ null, sqAxisZombies ],
    [ null, sqAxisZombies ]
];
axisZombiePlatoon.upgradesAllowed = false;


var axisFaction = new FactionDefinition(Faction.Axis);
axisFaction.upgrades = [ null, upgDefenses, upgExtraPanzerSupport, upgImplacable, upgAxisImprovedCommand, upgLightningWar, upgNebelwerferBarrage ];
axisFaction.platoons = [ axisSturmgrenadierePlatoon, axisSchwerPlatoon, axisBlutkreuzPlatoon, axisGorillaPlatoon, axisZombiePlatoon ];
axisFaction.heroes = [ hSigridVonThaler, hLara, hGrenadierX, hMarkus, hManfred, hAngela, hStefan, hTotenmeister ];



//  SSU
var upgSSUCavalryPlatoon = new UpgradeDefinition('CavP');
var upgSSUImprovedCommand = new UpgradeDefinition('SICm');
var upgSSUResourceful = new UpgradeDefinition('Rsrf');
var upgSSUPoliticalImportance = new UpgradeDefinition('PImp');
var upgSSUTankodesantniki = new UpgradeDefinition('Tnko');
var upgSSUTrainSupport = new UpgradeDefinition('TrnS');

var sqSSUCommand = new Squad('SCmS');
var sqSSUCommissar = new Squad('Cmsr');
var sqSSURedGuardsCommand = new Squad('RGCm');

var sqSSUBattleSquad = new Squad('BatS');
var sqSSUCloseCombat = new Squad('CCmS');
var sqSSURifleSquad = new Squad('RflS');
var sqSSUAuxiliaryAttack = new Squad('AuxS');
var sqSSURedGuardsAssaultSquad = new Squad('RdAs');
var sqSSURedGuardsAntitankSquad = new Squad('RdAt');

var sqSSUSniperTeam = new Squad('SSnT');
var sqSSUObserverTeam = new Squad('SObs');

var vhSSUKV47ANadya = new Squad('KV47A');
var vhSSUKV47BNatasha = new Squad('KV47B');
var vhSSUKV47CNatalya = new Squad('KV47C');

var vhSSUKV47DNina = new Squad('KV47D');
var vhSSUKV47ENikita = new Squad('KV47E');
var vhSSUKV47FNastasia = new Squad('KV47F');

var vhSSUAirborneTransport = new Squad('MI45');
var vhSSUAirborneWalkerTransport = new Squad('MI46');

var vhSSUMI47AStriker = new Squad('MI47A');
var vhSSUMI47BBurner = new Squad('MI47B');
var vhSSUMI47CAirblaster = new Squad('MI47C');
var vhSSUMI47DDeathRain = new Squad('MI47D');

var vhSSUIS5AMaoZedong = new Squad('IS5A');
var vhSSUIS5BVladimirLenin = new Squad('IS5B');

var vhSSUIS48AKarlMarx = new Squad('IS48A');
var vhSSUIS48BLavrentiyBeria = new Squad('IS48B');

var vhSSUKV3KMatrioshka = new Squad('KV3K');
var vhSSUKV3MBabushka = new Squad('KV3M');

var hKoshkaRudinova = new Squad('KshR');
var hKoshkaRudinovaWithGrandma = new Squad('KshRG');
var hNikolai = new Squad('Nkli');
var hYakov = new Squad('Ykov');
var hRedYana = new Squad('RYna');
var hWinterChild = new Squad('WChd');

var soloSSUCommissarFonvizin = new Squad('CFon');
var soloSSUCommissarSumarokov = new Squad('CSum');
var soloSSUCommissarDerzhavin = new Squad('CDer');
var soloSSUCommissarTrediakovsky = new Squad('CTrd');
var soloSSUCommissarKaramzin = new Squad('CKrm');

var ssuCommissars = [
    soloSSUCommissarFonvizin,
    soloSSUCommissarSumarokov,
    soloSSUCommissarDerzhavin,
    soloSSUCommissarTrediakovsky,
    soloSSUCommissarKaramzin
];

sqSSUCommand.numCommissars = 2;
sqSSUCommissar.supports = [ null, vhSSUAirborneTransport ];
//hKoshkaRudinova.supports = [ null, vhSSUGrandma ];

sqSSURedGuardsCommand.numCommissars = 2;

var ssuDefensePlatoon = new PlatoonDefinition('SDef');
ssuDefensePlatoon.sections = [
    [ sqSSUCommand, hKoshkaRudinova, hKoshkaRudinovaWithGrandma, hYakov ],
    [ sqSSUBattleSquad, sqSSUCloseCombat, sqSSURifleSquad ],
    [ null, sqSSUBattleSquad, sqSSUCloseCombat, sqSSUAuxiliaryAttack ],
    [ null, sqSSUBattleSquad, sqSSURifleSquad, sqSSUAuxiliaryAttack ],
    [ null, sqSSUCloseCombat, sqSSURifleSquad, sqSSUAuxiliaryAttack ]
];
ssuDefensePlatoon.supports = [
    sqSSUObserverTeam, sqSSUSniperTeam,
    vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUKV47CNatalya,
    vhSSUKV47DNina, vhSSUKV47ENikita, vhSSUKV47FNastasia,
    vhSSUAirborneTransport, vhSSUAirborneWalkerTransport,
    vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain,
    vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin,
    vhSSUIS48AKarlMarx, vhSSUIS48BLavrentiyBeria,
    vhSSUKV3KMatrioshka, vhSSUKV3MBabushka
];

var ssuPoliticalPlatoon = new PlatoonDefinition('SPol');
ssuPoliticalPlatoon.sections = [
    [ sqSSUCommissar, hNikolai ],
    [ sqSSUAuxiliaryAttack, sqSSUBattleSquad, sqSSUCloseCombat, sqSSURifleSquad ],
    [ null, sqSSUAuxiliaryAttack, sqSSUCloseCombat, sqSSURifleSquad ],
    [ null, sqSSUAuxiliaryAttack, sqSSUBattleSquad, sqSSURifleSquad ],
    [ null, sqSSUAuxiliaryAttack, sqSSUBattleSquad, sqSSUCloseCombat ]
];
ssuPoliticalPlatoon.supports = [
    sqSSUObserverTeam, sqSSUSniperTeam,
    vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUKV47CNatalya,
    vhSSUAirborneTransport,
    vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain,
    vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin,
    vhSSUKV3KMatrioshka, vhSSUKV3MBabushka
];

var ssuRedPlatoon = new PlatoonDefinition('SRed');
ssuRedPlatoon.sections = [
    [ sqSSURedGuardsCommand ],
    [ sqSSURedGuardsAntitankSquad, sqSSURedGuardsAssaultSquad ],
    [ null, sqSSURedGuardsAntitankSquad, sqSSURedGuardsAssaultSquad ],
    [ null, vhSSUKV3KMatrioshka, vhSSUKV3MBabushka,
        vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUKV47CNatalya,
        vhSSUKV47DNina, vhSSUKV47ENikita, vhSSUKV47FNastasia,
        vhSSUIS48AKarlMarx, vhSSUIS48BLavrentiyBeria ],
    [ null, vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUKV47CNatalya,
        vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin,
        vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain,
        vhSSUAirborneWalkerTransport ]
];

ssuRedPlatoon.supports = [
    sqSSUObserverTeam, sqSSUSniperTeam,
    sqSSUBattleSquad, sqSSUCloseCombat, sqSSURifleSquad
];

var ssuFaction = new FactionDefinition(Faction.SSU);
ssuFaction.upgrades = [ null, upgSSUCavalryPlatoon, upgSSUImprovedCommand, upgSSUResourceful, upgSSUPoliticalImportance, upgSSUTankodesantniki, upgSSUTrainSupport ];
ssuFaction.platoons = [ ssuDefensePlatoon, ssuPoliticalPlatoon, ssuRedPlatoon ];
ssuFaction.heroes = [ hKoshkaRudinova, hKoshkaRudinovaWithGrandma, hNikolai, hYakov, hRedYana, hWinterChild ];



var Factions = [];
Factions[Faction.Allies] = alliesFaction;
Factions[Faction.Axis] = axisFaction;
Factions[Faction.SSU] = ssuFaction;



function Encoder(buf)
{
    this.buffer = buf || "";
    this.val = 0;
    this.bits = 0;
    this.chars = 0;
    this.codex = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    this.EncodeInt = function(i, bits)
    {
        if (i < 0) i = 0; // No negative numbers allowed

        this.val = this.val << bits;
        this.val |= i & ((1 << bits) - 1);
        this.bits += bits;

        while (this.bits >= 6)
        {
            var b = (this.val >> (this.bits - 6)) & 0x3F;
            this.buffer += this.codex.charAt((b + (this.buffer.length*3)) % this.codex.length);
            this.bits -= 6;
            this.val = this.val & ((1 << this.bits) - 1);   //  Optional: remove the bits we encoded
        }
    }

    this.EncodeBool = function(b) { this.EncodeInt(b ? 1 : 0, 1); return b; }

    this.EncodeString = function(str)
    {
        var bytes = [],
            offset = 0;

        str = encodeURI(str);
        var length = str.length;

        while (offset < length)
        {
            var char = str[offset];
            offset += 1;

            if ('%' !== char)
                this.EncodeInt(char.charCodeAt(0), 8);
            else
            {
                this.EncodeInt(parseInt(str.substr(offset, 2), 16), 8);
                offset += 2;
            }
        }

        this.EncodeInt(0, 8);
    }

    this.FinishEncode = function()
    {
        if (this.bits > 0)
        {
            this.val = this.val << (6 - this.bits);
            var b = (this.val >> (this.bits - 6)) & 0x3F;
            this.buffer += this.codex.charAt((b + (this.buffer.length*3)) % this.codex.length);

            this.buffer += this.codex.charAt(this.val & 0x3F);
            this.val = 0;
            this.bits = 0;
        }
    }

    this.DecodeInt = function(bits)
    {
        while (this.bits < bits)
        {
            var val = (this.codex.indexOf(this.buffer.charAt(0)) - (this.chars*3));
            while (val < 0) val += this.codex.length;

            this.val = this.val << 6;
            this.val = this.val | val;

            this.buffer = this.buffer.substr(1);

            this.chars += 1;
            this.bits += 6;
        }
        this.bits -= bits;
        var val = (this.val >> this.bits) & ((1 << bits) - 1);
        this.val = this.val & ((1 << this.bits) - 1);   //  Optional: remove the bits we encoded
        return val;
    }

    this.DecodeBool = function() { return this.DecodeInt(1) == 1; }

    this.DecodeString = function()
    {
        var chars = [],
            offset = 0;

        while (1) {
            var c = this.DecodeInt(8);

            if (c == 0)
                break;
            else if (c < 128)
            {
                chars.push(String.fromCharCode(c));
                offset += 1;
            }
            else if (c > 191 && c < 224)
            {
                var c2 = this.DecodeInt(8);
                chars.push(String.fromCharCode(((c & 31) << 6) | (c2 & 63)));
                offset += 2;
            }
            else
            {
                var c2 = this.DecodeInt(8),
                    c3 = this.DecodeInt(8);
                chars.push(String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)));
                offset += 3;
            }
        }

        return chars.join('');
    }

    this.ToString = function()
    {
        this.FinishEncode();
        return this.buffer;
    }

    this.Good = function()
    {
        return this.buffer.length > 0 && this.bits > 0;
    }
}


function ReencodeForceFromURIParam(hash)
{
    //  Version = 1
    var decoder = new Encoder(hash.substr(1));
    var encoder = new Encoder();

    //  Version
    decoder.DecodeInt(4);   //  Ignore old version
    encoder.EncodeInt(2, 4);    //  Version -> 2

    //  Faction
    var factionId = decoder.DecodeInt(3);
    encoder.EncodeInt(factionId, 3);
    var faction = Factions[factionId];

    //  Points
    var forcePoints = decoder.DecodeInt(12);
    encoder.EncodeInt(forcePoints, 12);

    var bNotes = decoder.DecodeBool();
    if (encoder.EncodeBool(bNotes))
    {
        var notes = decoder.DecodeString();
        encoder.EncodeString(notes);
    }

    var numHeroes = decoder.DecodeInt(5);
    var numPlatoons = decoder.DecodeInt(5);

    //  Heroes
    encoder.EncodeInt(numHeroes, 5);
    for (var h = 0; h < numHeroes; ++h)
    {
        var heroIdx = decoder.DecodeInt(6);
        var hero = faction.heroes[heroIdx];
        encoder.EncodeString(hero.id);
    }

    //  Platoons
    encoder.EncodeInt(numPlatoons, 5);
    for (var p = 0; p < numPlatoons; ++p)
    {
        var platoonIdx = decoder.DecodeInt(4);
        var platoon = faction.platoons[platoonIdx];
        encoder.EncodeString(platoon.id);

        bNotes = decoder.DecodeBool();
        if (encoder.EncodeBool(bNotes))
        {
            var notes = decoder.DecodeString();
            encoder.EncodeString(notes);
        }

        var upgradeIdx = decoder.DecodeInt(6);
        var upgrade = faction.upgrades[upgradeIdx];
        if (encoder.EncodeBool(upgrade != null))
        {
            encoder.EncodeString(upgrade.id);
        }

        var sections = [];
        for (var s = 0; s < NumSections; ++s)
        {
            var unitIdx = decoder.DecodeInt(4);
            var unit = platoon.sections[s][unitIdx];
            sections[s] = unit;
            if (encoder.EncodeBool(unit != null))
            {
                encoder.EncodeString(unit.id); //  Unit
                encoder.EncodeBool(false);  //  Upgrade
            }
        }

        if (faction == ssuFaction)
        {
            var numCommissars = decoder.DecodeInt(3);
            encoder.EncodeInt(numCommissars);
            for (var c = 0; c < numCommissars; ++c)
            {
                var unitIdx = decoder.DecodeInt(3);
                var unit = ssuCommissars[unitIdx];
                encoder.EncodeString(unit.id);
            }
        }

        var numSupports = decoder.DecodeInt(3);
        encoder.EncodeInt(numSupports, 3);
        for (var s = 0; s < numSupports; ++s)
        {
            var bResourceful = false;
            if (upgrade == upgSSUResourceful)
            {
                bResourceful = decoder.DecodeBool();
            }

            var unitIdx = decoder.DecodeInt(5);
            var unit = bResourceful ? platoon.sections[1][unitIdx]
                                    : platoon.supports[unitIdx];

            encoder.EncodeString(unit.id);
            encoder.EncodeBool(false);      //  Unit upgrade
        }

        while (1)
        {
            var bonusSupportType = decoder.DecodeInt(2);
            var section = 0;
            var unit = null;

            if (bonusSupportType == 0)
                break;
            else if (bonusSupportType == 1)
            {
                //  Upgrade
                unitIdx = decoder.DecodeInt(5);
                unit = upgrade.computeSupports(platoon)[unitIdx];
            }
            else if (bonusSupportType == 2)
            {
                //  Section
                section = decoder.DecodeInt(3);
                unitIdx = decoder.DecodeInt(5);
                unit = sections[section].supports[unitIdx];
            }

            if (unit)
            {
                encoder.EncodeBool(true);
                if (bonusSupportType == 1)
                {
                    encoder.EncodeInt(0, 2);
                }
                else if (bonusSupportType == 2)
                {
                    encoder.EncodeInt(1, 2);
                    encoder.EncodeInt(section, 3);
                }
                encoder.EncodeString(unit.id);
                encoder.EncodeBool(false);
            }
        }
        encoder.EncodeBool(false);
    }

    return '#' + encoder.ToString();
}


function Load()
{
    if (window.location.hash)
    {
        var uri = ReencodeForceFromURIParam(window.location.hash);
        window.open('index.html?' + uri, '_self');
    }
}
