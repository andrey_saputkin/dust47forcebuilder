/**
 * Code is Copyright 2012-13 Chris Jacobson
 */

var Revision = 33;

var Factions = [];
Factions[Faction.Allies] = alliesFaction;
Factions[Faction.Axis] = axisFaction;
Factions[Faction.SSU] = ssuFaction;
var maxFaction = Faction.SSU;

var Force = {};
Force.faction = null;
Force.platoons = [];
Force.heroes = [];
Force.points = 0;
Force.notes = "";
Force.maxPoints = 300;
Force.numHeroes = 0;
Force.maxHeroes = 0;
Force.numPlatoons = 0;
Force.maxPlatoons = 0;
Force.errors = [];
Force.lastError = '';
Force.LinkURI = "";

function Unit(definition)
{
    this.definition = definition;
    this.upgrade = null;

    this.Points = function()
    {
        var points = this.definition.points;
        if (this.upgrade) points += this.upgrade.points;
        return points;
    }
}


function Platoon(definition)
{
    this.definition = definition;
    this.upgrade = null;
    this.order = 0;

    this.sections = [];
    for (var s = 0; s < NumSections; ++s)
    {
        this.sections[s] = null;
    }

    this.notes = "";
    this.supports = [];
    this.numSupports = 0;
    this.maxSupports = 0;
    this.additionalSupports = 0;
    this.bonusSupports = {};
    this.commissars = [];
    this.numCommissars = 0;
    this.numCapturedVehicles = 0;

    this.points = 0;
}



function GETV(e) { return (e.srcElement ? e.srcElement.value : e.target.value); }


function Template(str, dict)
{
    return str.replace(/{{[^}]+}}/g,
        function(key)
        {
            key = key.replace(/[{}]+/g, "");
            var val = dict[key];
            return val != null ? val.toString() : '';
        }
    );
}

function RemoveItemByIndex(array, idx)
{
    //  because splice sucks
    var newArray = [];
    for (var i in array)
    {
        if (i != idx)
        {
            newArray[i] = array[i];
        }
    }
    return newArray;
}


function ForceError(error)
{
    Force.lastError = error;
    Force.errors.push(error);
}


function MakeNameWithPoints(definition)
{
    var name = definition ? definition.name : 'None';
    if (definition && definition.points)
    {
        name = name + ' (' + definition.points + ')';
    }

    return name;
}

function MakeSelectOptions(select, options, def)
{
    def = def || select.val();

    select.empty();
    for (var o in options)
    {
        var option = options[o];

        select.append(
            $('<option/>', { 'value': option ? option.id : '' })
                .text(MakeNameWithPoints(option))
        );
    }

    if (def)
    {
        select.val(def);
    }
}


function MakeDeleteButton(clickFunc)
{
//    return $('<button/>')
//        //.addClass('small')
//        .addClass('close').attr('style', 'float:none')
//        .attr('onclick', clickFunc)
//        .append('&times;');
    return $('<a/>')
        .addClass('close').attr('style', 'float:none')
        .attr('onclick', clickFunc)
        .append('&times;');
}

function MakeAddButton(clickFunc)
{
    return $('<button/>')
        .addClass('small')
        //.addClass('close').attr('style', 'float:left')
        .attr('onclick', clickFunc)
        .append('+');
}

function MakeUpgradeButton(id, clearFunc)
{
    return $('<i class="icon-chevron-right upgradeBtn"/>')
        .data('clear', clearFunc)
        .click(function() { ToggleUnitUpgradeVis(id); })
}

function MakePoints(label)
{
    return $('<span/>').addClass('badge points').append(
        $('<span/>').addClass('value'),
        label || '',
        $('<i/>', { 'style': 'display:none' }).addClass('icon-warning-sign')
    );
}

function UpdatePoints(root, points)
{
    $('#' + root + ' .points')
        .toggleClass('badge-success', points > 0)
        .children('.value')
            .text(points.toString());
}

function UpdatePointsTooltip(root, points, valid, tooltipText)
{
    UpdatePoints(root, points);

    $('#' + root + ' .points')
        .toggleClass('badge-warning', !valid)
        .toggleClass('badge-success', valid && points > 0)
        .children('.icon-warning-sign')
            .toggle(!valid)
            .attr('data-original-title', tooltipText || Force.lastError)
            .tooltip('fixTitle');
}



function SetFaction(factionId, reset, manualSelect)
{
    if (!Force.faction || Force.faction.faction != factionId || reset)
    {
        Force.faction = Factions[factionId];
        Force.platoons = [];
        Force.heroes = [];

        Force.numPlatoons = 0;
        Force.numHeroes = 0;

	//Define capturable vehicles here
	if(factionId == Faction.Allies){
	    Force.capturableVehicles = [null].concat(vhAxisCapturable, vhSSUCapturable);
	} else if (factionId == Faction.Axis){
	    Force.capturableVehicles = [null].concat(vhAlliesCapturable, vhSSUCapturable);
	} else if (factionId == Faction.SSU){
	    Force.capturableVehicles = [null].concat(vhAlliesCapturable, vhAxisCapturable);
	}

        $('#faction select').val(factionId);

        $('#platoons').empty();
        $('#heroes').empty();

        AddPlatoon();

        if (manualSelect)
        {
            SetCookie('faction', Force.faction.id.toString(), 30);
        }
    }
}


function SetForceSize(size, manualSelect)
{
    Force.maxPoints = size;

    $('#force-size').val(Force.maxPoints.toString());

    if (manualSelect)
    {
        SetCookie('size', Force.maxPoints.toString(), 30);
    }
}


function SetForceNotes()
{
    Force.notes = $.trim($('#force .notes').val());
}


function MakePlatoonId(platoonId) { return 'platoon' + platoonId; }
function MakeUpgradeId(platoonId) { return MakePlatoonId(platoonId) + ' .platoonupgrade'; }
function MakeSectionId(platoonId, section) { return MakePlatoonId(platoonId) + ' .section' + section; }
function MakeSupportId(platoonId, support) { return MakePlatoonId(platoonId) + ' .support' + support; }
function MakeBonusSupportId(platoonId, type, support) { return MakePlatoonId(platoonId) + ' .support-' + type + (support != null ? support : ''); }
function MakeHeroId(hero) { return 'hero' + hero; }
function MakeCommissarId(platoonId, commissar) { return MakePlatoonId(platoonId) + ' .commissar' + commissar; }


function AddPlatoon(platoonTypeId)
{
    var platoon = new Platoon(Force.faction.platoons[0]);

    var platoonId = Force.platoons.length;
    Force.platoons[platoonId] = platoon;
    Force.numPlatoons += 1;

    platoon.order = Force.numPlatoons;

    var id = MakePlatoonId(platoonId);

    $('<li/>', { 'id': id }).addClass('thumbnail ui-widget-content').data("id", platoonId)
        .append(
        //  Header
        $('<div class="row-fluid"/>').append(
            $('<div class="span5 form-inline"/>').append(
                $('<label/>').addClass('control-group').append(
                    MakeDeleteButton('RemovePlatoon(' + platoonId + ');UpdateForce();'),
                    $('<b></b>').text('Platoon: ')
                ),
                $('<select class="input-xlarge platoontype"/>')
                    .attr('onchange', 'SetPlatoonType(' + platoonId + ',GETV(event));UpdateForce();')
            ),
            $('<div class="span1 platoonpoints" align="center"/>').append(
                MakePoints(' Points')
            ),
            $('<div class="span6" align="right"/>').append(
                $('<b>Name: </b>'),
                $('<input class="input-xlarge notes" type="text"/>')
                    .attr('onblur', 'SetPlatoonNote(' + platoonId + ');UpdateForce();'),
                ' ',
                $('<i class="icon-arrow-up move-up" onclick="MovePlatoonUp(' + platoonId + ');"/>'),
                $('<i class="icon-arrow-down move-down" onclick="MovePlatoonDown(' + platoonId + ');"/>'),
                ' | ',
                $('<i/>', { 'data-toggle': 'collapse',  'href': '#' + id + ' .collapse'})
                    .addClass('icon-chevron-down collapsebutton')
            )
       ),
        //  Sections
        $('<div/>').addClass('row-fluid collapse in')
            .on('hide', function () { $('#' + id + ' .collapsebutton').toggleClass('icon-chevron-down icon-chevron-up'); })
            .on('show', function () { $('#' + id + ' .collapsebutton').toggleClass('icon-chevron-up icon-chevron-down'); })
            .append(
            //  Left Column
            $('<div class="span6"/>').append(
                $('<table width="100%" class="table-bordered table-striped"/>').append(
                    $('<colgroup/>').append(
                        $('<col width="40%"/>'),
                        $('<col width="50%"/>'),
                        $('<col width="10%"/>')
                    ),
                    //  Sections
                    $('<tbody class="sections"/>')
                )
            ),
            //  Right Column
            $('<div/>').addClass('span6').append(
                $('<table width="100%" class="table-bordered"/>').append(
                    $('<colgroup/>').append(
                        $('<col width="40%"/>'),
                        $('<col width="50%"/>'),
                        $('<col width="10%"/>')
                    ),
                    //  Platoon Upgrade
                    $('<tr class="platoonupgrade"/>').append(
                        $('<th align="right"/>').text('Upgrade:'),
                        $('<td/>').append(
                            $('<select class="input-xlarge"/>')
                                .attr('onchange', 'SetPlatoonUpgrade(' + platoonId + ',GETV(event));UpdateForce();')
                        ),
                        $('<td align="center"/>').append(
                            MakePoints()
                        )
                    ),
                    //  Commissars
                    $('<tbody class="commissars"/>').append(
                        $('<tr class="addCommissar"/>').append(
                            $('<td/>'),
                            $('<td colspan="2"/>').append(
                                $('<b/>').append(
                                    'Commissars ( ',
                                    $('<span class="numCommissars"/>'),
                                    ' / ',
                                    $('<span class="maxCommissars"/>'),
                                    ' ) '
                                ),
                                MakeAddButton('AddCommissar(' + platoonId + ');UpdateForce();')
                            )
                        )
                    ),
                    //  Support
                    $('<tbody class="bonussupports"/>'),
                    $('<tr class="addSupport"/>').append(
                        $('<td/>'),
                        $('<td colspan="2"/>').append(
                            $('<b/>').append(
                                'Supports ( ',
                                $('<span class="numSupports"/>'),
                                ' / ',
                                $('<span class="maxSupports"/>'),
                                ' ) '
                            ),
                            MakeAddButton('AddSupport(' + platoonId + ');UpdateForce();')
                        )
                    ),
                    $('<tbody class="supports"/>')
                )
            )
        )
    ).appendTo('#platoons');

    //  Insert Sections
    for (var s = 0; s < NumSections; ++s)
    {
        $('#' + id + ' .sections').append(
            $('<tr/>').addClass('section' + s).append(
                $('<td align="right"/>').append(
                    $('<b/>').append(SectionName[s] + ' Section'),
                    MakeUpgradeButton(
                        MakeSectionId(platoonId, s),
                        (function(p, s) { return function() { SetSectionUpgrade(p, s, ''); } })(platoonId, s))
                ),
                $('<td/>').append(
                    $('<div class="unit"/>').append(
                        $('<select class="input-xlarge"/>')
                            .attr('onchange', 'SetSection(' + platoonId + ',' + s + ',GETV(event));UpdateForce();')
                    ),
                    $('<div class="upgrade"/>').append(
                        'Upgrade: ',
                        $('<select class="input-large"/>')
                            .attr('onchange', 'SetSectionUpgrade(' + platoonId + ',' + s + ',GETV(event));UpdateForce();')
                    )
                ),
                $('<td align="center"/>').append(
                    MakePoints()
                )
            )
        );
    }

    //  SSU Commissars
    if (Force.faction != ssuFaction)
    {
        $('#' + MakePlatoonId(platoonId) + ' .commissars').hide();
    }

    MakeSelectOptions(
        $('#' + id + ' .platoontype'),
        Force.faction.platoons,
        platoon.definition.id);

    platoonTypeId = platoonTypeId || Force.faction.platoons[0];
    SetPlatoonType(platoonId, platoonTypeId);

    return platoonId;
}


function RemovePlatoon(platoonId)
{
    //  No removing last platoon
    if (Force.numPlatoons == 1)
    {
        return;
    }

    var platoon = Force.platoons[platoonId];

    for (var p in Force.platoons)
    {
        var otherPlatoon = Force.platoons[p];
        if (otherPlatoon.order > platoon.order)
        {
            --otherPlatoon.order;
        }
    }

    Force.platoons = RemoveItemByIndex(Force.platoons, platoonId);

    $('#' + MakePlatoonId(platoonId)).remove();
}

function OnPlatoonsMoved()
{
    var order = 1;

    $('#platoons').children('li').each(
        function() {
            var platoonId = $(this).data("id");
            var platoon = Force.platoons[platoonId];
            platoon.order = order++;
        }
    );

    EncodeForceIntoURIParam();
}

function MovePlatoonUp(platoonId)
{
    var platoon = Force.platoons[platoonId];

    if (platoon.order == 1) return;

    var platoonElement = $('#' + MakePlatoonId(platoonId));
    platoonElement.slideUp(function(){
        $(this).insertBefore(platoonElement.prev()).slideDown();
        OnPlatoonsMoved();
    });
}


function MovePlatoonDown(platoonId)
{
    var platoon = Force.platoons[platoonId];

    if (platoon.order >= Force.platoons.length) return;

    var platoonElement = $('#' + MakePlatoonId(platoonId));
    platoonElement.slideUp(function(){
        $(this).insertAfter($(this).next()).slideDown();
        OnPlatoonsMoved();
    });
}

function SetPlatoonType(platoonId, platoonTypeId)
{
    var platoon = Force.platoons[platoonId];

    for (var p in Force.faction.platoons)
    {
        if (Force.faction.platoons[p].id == platoonTypeId)
        {
            platoon.definition = Force.faction.platoons[p];
            break;
        }
    }

    $('#' + MakePlatoonId(platoonId) + ' .platoontype').val(platoon.definition.id);

    if (platoon.definition.upgradesAllowed)
    {
    	//Handle platoon defined limited upgrades
        var tempUpgrades = [];
        var usingLimitedUpgrades = false;
        if(platoon.definition.limitedUpgrades.length > 0){
        	usingLimitedUpgrades = true;
        	tempUpgrades = platoon.definition.limitedUpgrades;
        } else {
        	tempUpgrades = Force.faction.upgrades;
        }
    	
        var upgrades = [];
        for (var o in tempUpgrades)
        {
            var upgrade = tempUpgrades[o];

            //If using limited platoon upgrades, use them all...
            if (!upgrade || usingLimitedUpgrades || upgrade.prevalidate(platoon))
            {
                upgrades.push(upgrade);
            }
        }

        platoon.upgrade = upgrades[0];

        MakeSelectOptions(
            $('#' + MakeUpgradeId(platoonId) + ' select'),
            upgrades,
            platoon.upgrade ? platoon.upgrade.id : '');

        $('#' + MakeUpgradeId(platoonId)).show();
    }
    else
    {
        $('#' + MakeUpgradeId(platoonId)).hide();
        platoon.upgrade = null;
    }

    platoon.supports = [];
    $('#' + MakePlatoonId(platoonId) + ' .supports').empty();
    $('#' + MakePlatoonId(platoonId) + ' .addSupport').toggle(platoon.definition.supports.length != 0);

    platoon.bonusSupports = {};
    $('#' + MakePlatoonId(platoonId) + ' .bonussupports').empty();

    var s = 0;

    //Add the option for a capture vehicle
    AddBonusSupport(platoonId, 'captured', s, Force.capturableVehicles);

    for (s = 0; s < NumSections; ++s)
    {
        var sectionUnits = platoon.definition.sections[s];
        var sectionId = MakeSectionId(platoonId, s);

        MakeSelectOptions(
            $('#' + sectionId + ' .unit select'),
            sectionUnits);

        $('#' + sectionId + ' .upgrade').hide();

        SetSection(platoonId, s, sectionUnits[0] ? sectionUnits[0].id : '');
    }

    for (s in platoon.definition.additionalSupports)
    {
        AddBonusSupport(platoonId, 'additional', s, platoon.definition.additionalSupports[s]);
    }

    SetPlatoonUpgrade(platoonId, 0);

}


function SetPlatoonUpgrade(platoonId, upgradeId)
{
    var platoon = Force.platoons[platoonId];

    $('#' + MakeUpgradeId(platoonId) + ' select').val(upgradeId);

    if (platoon.upgrade && platoon.upgrade.supports.length)
    {
        platoon.bonusSupports['upgrade' + 0] = null;
        $('#' + MakeBonusSupportId(platoonId, 'upgrade', 0)).remove();
    }

    if (platoon.upgrade == upgSSUResourceful)
    {
        platoon.upgrade = null;

        for (var s in platoon.supports)
        {
            var support = platoon.supports[s];
            if ($.inArray(support.definition, platoon.definition.supports) < 0)
            {
                SetSupport(platoonId, s, 0);
            }

            SetSupportOptions(platoonId, s);
        }
    }

    //Handle platoon defined limited upgrades
    var tempUpgrades = [];
    if(platoon.definition.limitedUpgrades.length > 0){
    	tempUpgrades = platoon.definition.limitedUpgrades;
    } else {
    	tempUpgrades = Force.faction.upgrades;
    }
    
    platoon.upgrade = tempUpgrades[0];
    for (var u in tempUpgrades)
    {
        var upgrade = tempUpgrades[u];
        var thisUpgradeId = upgrade ? upgrade.id : '';

        if (thisUpgradeId == upgradeId)
        {
            platoon.upgrade = upgrade;
            break;
        }
    }

    if (platoon.upgrade && platoon.upgrade.supports.length)
    {
        AddBonusSupport(platoonId, 'upgrade', 0, platoon.upgrade.computeSupports(platoon));
    }

    UpdatePointsTooltip(MakeUpgradeId(platoonId), platoon.upgrade ? platoon.upgrade.points : 0, true, '');    //  Always valid for now

    if (platoon.upgrade == upgSSUResourceful)
    {
        for (s in platoon.supports)
        {
            SetSupportOptions(platoonId, s);
        }
    }
}


function SetPlatoonNote(platoonId)
{
    var platoon = Force.platoons[platoonId];

    platoon.notes = $.trim($('#' + MakePlatoonId(platoonId) + ' .notes').val());
}


function SetSection(platoonId, section, unitId)
{
    var platoon = Force.platoons[platoonId];
    var id = MakeSectionId(platoonId, section);

    $('#' + id + ' .unit select').val(unitId);

    var oldUnit = platoon.sections[section];
    var newUnitDefinition = Units[unitId];
    var unit = newUnitDefinition ? new Unit(newUnitDefinition) : null;

    if (oldUnit && oldUnit.definition.supports.length)
    {
        // Remove the bonus support
        platoon.bonusSupports['section' + section] = null;
        $('#' + MakeBonusSupportId(platoonId, 'section', section)).remove();
    }
    platoon.sections[section] = unit;
    if (unit && unit.definition.supports.length)
    {
        AddBonusSupport(platoonId, 'section', section, unit.definition.supports);
    }

    ResetUnitUpgrade(id, unit);

    UpdatePoints(id, unit ? unit.Points() : 0);
}


function SetSectionUpgrade(platoonId, section, upgId)
{
    var platoon = Force.platoons[platoonId];
    var unit = platoon.sections[section];
    if (!unit) return;

    var id = MakeSectionId(platoonId, section);
    $('#' + id + ' .upgrade select').val(upgId);

    unit.upgrade = Upgrades[upgId];

    UpdatePoints(id, unit.Points());
}

//noinspection JSUnusedGlobalSymbols
function AddSupport(platoonId, unitId)
{
    var platoon = Force.platoons[platoonId];
    var supportId = platoon.supports.length;

    $('<tr/>').addClass('support' + supportId).append(
        $('<th align="right"/>').append(
            MakeDeleteButton('RemoveSupport(' + platoonId + ',' + supportId + ');UpdateForce();'),
            'Support',
            MakeUpgradeButton(
                MakeSupportId(platoonId, supportId),
                function() { SetSupportUpgrade(platoonId, supportId, ''); })
        ),
        $('<td valign="middle"/>').append(
            $('<div class="unit"/>').append(
                $('<select/>').addClass('input-xlarge')
                    .attr('onchange', 'SetSupport(' + platoonId + ',' + supportId + ',GETV(event));UpdateForce();')
            ),
            $('<div class="upgrade"/>').append(
                'Upgrade: ',
                $('<select/>').addClass('input-large')
                    .attr('onchange', 'SetSupportUpgrade(' + platoonId + ',' + supportId + ',GETV(event));UpdateForce();')
            )
        ),
        $('<td align="center"/>').append(
            MakePoints()
        )
    ).appendTo($('#' + MakePlatoonId(platoonId) + ' .supports'));

    unitId = SetSupportOptions(platoonId, supportId, unitId);
    SetSupport(platoonId, supportId, unitId);

    return supportId;
}


function SetSupportOptions(platoonId, supportId, unitId)
{
    var platoon = Force.platoons[platoonId];
    var supports = platoon.definition.supports;

    if (platoon.upgrade == upgSSUResourceful)
    {
        supports = supports.concat(platoon.definition.sections[1]);
    }

    var bFound = false;
    for (var s in supports)
    {
        var support = supports[s];

        if (support.id == unitId)
        {
            bFound = true;
            break;
        }
    }

    if (!bFound)
    {
        unitId = null;
    }

    unitId = unitId || supports[0].id;

    MakeSelectOptions(
        $('#' + MakeSupportId(platoonId, supportId) + ' .unit select'),
        supports,
        unitId);

    return unitId;
}


function AddBonusSupport(platoonId, type, supportId, supports, unitId)
{
    var label = " Bonus Support";
    if (type == 'upgrade') label = Force.platoons[platoonId].upgrade.name;
    else if (type == 'section') label = SectionName[supportId] + " Section Support";
    else if (type == 'additional') label = 'Additional ' + supportId;
    else if (type == 'captured') label = 'Captured Vehicle';

    $('<tr/>').addClass('support-' + type + supportId).append(
        $('<th align="right"/>').append(
            label,
            MakeUpgradeButton(
                MakeBonusSupportId(platoonId, type, supportId),
                function() { SetBonusSupportUpgrade(platoonId, type, supportId, ''); })
        ),
        $('<td valign="middle"/>').append(
            $('<div class="unit"/>').append(
                $('<select/>').addClass('input-xlarge')
                    .attr('onchange', 'SetBonusSupport(' + platoonId + ',"' + type + '","' + supportId + '",GETV(event));UpdateForce();')
            ),
            $('<div class="upgrade"/>').append(
                'Upgrade: ',
                $('<select/>').addClass('input-large')
                    .attr('onchange', 'SetBonusSupportUpgrade(' + platoonId + ',"' + type + '","' + supportId + '",GETV(event));UpdateForce();')
            )
        ),
        $('<td align="center"/>').append(
            MakePoints()
        )
    ).appendTo('#' + MakePlatoonId(platoonId) + ' .bonussupports');

    MakeSelectOptions(
        $('#' + MakeBonusSupportId(platoonId, type, supportId) + ' .unit select'),
        supports);

    unitId = unitId || (supports[0] ? supports[0].id : '');
    SetBonusSupport(platoonId, type, supportId, unitId);
}


//noinspection JSUnusedGlobalSymbols
function AddCommissar(platoonId, unitId)
{
    var commissarId = Force.platoons[platoonId].commissars.length;

    $('<tr/>').addClass('commissar' + commissarId).append(
        $('<th/>', { 'align': 'right' }).append(
            MakeDeleteButton('RemoveCommissar(' + platoonId + ',' + commissarId + ');UpdateForce();'),
            'Commissar: '
        ),
        $('<td/>', { 'valign': 'middle' }).append(
            $('<select/>').addClass('input-xlarge')
                .attr('onchange', 'SetCommissar(' + platoonId + ',' + commissarId + ',GETV(event));UpdateForce();')
        ),
        $('<td/>', { 'valign': 'middle' }).append(
            MakePoints()
        )
    ).appendTo('#' + MakePlatoonId(platoonId) + ' .commissars');

    MakeSelectOptions(
        $('#' + MakeCommissarId(platoonId, commissarId) + ' select'),
        ssuCommissars);

    SetCommissar(platoonId, commissarId, unitId || ssuCommissars[0].id);

    return commissarId;
}


function RemoveSupport(platoonId, supportId)
{
    var platoon = Force.platoons[platoonId];
    platoon.supports = RemoveItemByIndex(platoon.supports, supportId);

    $('#' + MakeSupportId(platoonId, supportId)).remove();
}


function RemoveCommissar(platoonId, commissarId)
{
    var platoon = Force.platoons[platoonId];
    platoon.commissars = RemoveItemByIndex(platoon.commissars, commissarId);

    $('#' + MakeCommissarId(platoonId, commissarId)).remove();
}


function SetSupport(platoonId, supportId, unitId)
{
    var platoon = Force.platoons[platoonId];
    var unitDefinition = Units[unitId];
    var unit = unitDefinition ? new Unit(unitDefinition) : null;

    platoon.supports[supportId] = unit;

    var id = MakeSupportId(platoonId, supportId);

    ResetUnitUpgrade(id, unit);

    UpdatePoints(id, unit ? unit.Points() : 0);
}


function SetSupportUpgrade(platoonId, supportId, upgId)
{
    var platoon = Force.platoons[platoonId];
    var unit = platoon.supports[supportId];
    if (!unit) return;

    var id = MakeSupportId(platoonId, supportId);
    $('#' + id + ' .upgrade select').val(upgId);

    unit.upgrade = Upgrades[upgId];

    UpdatePoints(id, unit.Points());
}


function SetBonusSupport(platoonId, type, supportId, unitId)
{
    var platoon = Force.platoons[platoonId];

    var id = MakeBonusSupportId(platoonId, type, supportId);
    $('#' + id + ' .unit select').val(unitId);

    var support = Units[unitId];
    var unit = support ? new Unit(support) : null;
    platoon.bonusSupports[type + supportId] = unit;

    ResetUnitUpgrade(id, unit);

    if(type == 'captured'){
    	UpdatePoints(id, unit ? unit.Points() + capturedExtraPointsCost : 0);
    } else {
        UpdatePoints(id, unit ? unit.Points() : 0);
    }

}


function SetBonusSupportUpgrade(platoonId, type, supportId, upgId)
{
    var platoon = Force.platoons[platoonId];
    var unit = platoon.bonusSupports[type + supportId];
    if (!unit) return;

    var id = MakeBonusSupportId(platoonId, type, supportId);
    $('#' + id + ' .upgrade select').val(upgId);

    unit.upgrade = Upgrades[upgId];

    if(type == 'captured'){
    	UpdatePoints(id, unit.Points() + capturedExtraPointsCost);
    } else {
        UpdatePoints(id, unit.Points());
    }
}

function SetCommissar(platoonId, commissarId, unitId)
{
    var commissar = Units[unitId];
    var unit = commissar ? new Unit(commissar) : null;
    Force.platoons[platoonId].commissars[commissarId] = unit;

    $('#' + MakeCommissarId(platoonId, commissarId) + ' select').val(unitId);

    UpdatePoints(MakeCommissarId(platoonId, commissarId), unit ? unit.Points() : 0);
}


function AddHero(unitId)
{
    var heroId = Force.heroes.length;
    var id = MakeHeroId(heroId);

    //  <row-fluid><button>Hero: <select><points></row-fluid>
    $('<div/>', {'id': id}).addClass('row-fluid').append(
        $('<div class="unit"/>').append(
            MakeDeleteButton('RemoveHero(' + heroId + ');UpdateForce();'),
            MakeUpgradeButton(id, function() { SetHeroUpgrade(heroId, ''); } ),
            $('<b/>').append('Hero: '),
            $('<select/>').addClass('hero')
                .attr('onchange', 'SetHero(' + heroId + ',GETV(event));UpdateForce();'),
            MakePoints()
        ),
        $('<div class="upgrade"/>').append(
            'Upgrade:',
            $('<select/>').addClass('input-large')
                .attr('onchange', 'SetHeroUpgrade(' + heroId + ',GETV(event));UpdateForce();')
        )
    ).appendTo($('#heroes'));

    unitId = unitId || Force.faction.heroes[0].id;

    //  Populate the Select
    MakeSelectOptions(
        $('#' + id + ' .unit select'),
        Force.faction.heroes,
        unitId);

    SetHero(heroId, unitId);
}


function RemoveHero(heroId)
{
    Force.heroes = RemoveItemByIndex(Force.heroes, heroId);

    $('#' + MakeHeroId(heroId)).remove();
}


function SetHero(heroId, unitId)
{
    var hero = new Unit(Units[unitId]);
    Force.heroes[heroId] = hero;

    ResetUnitUpgrade(MakeHeroId(heroId), hero);

    UpdatePoints(MakeHeroId(heroId), hero.Points());
}


function ResetUnitUpgrade(id, unit)
{
    var upgrades = [];

    if (unit)
    {
        upgrades = upgrades.concat(unit.definition.upgrades);

        if (unit.definition.type == UnitType.Soldier &&
            unit.definition.upgradesAllowed)
        {
            upgrades = upgrades.concat(upgUnitGeneral);
            upgrades = upgrades.concat(Force.faction.unitUpgrades);
        }
    }

    if (upgrades.length == 0)
    {
        $('#' + id + ' .upgradeBtn').hide();
        $('#' + id + ' .upgrade select').html('');
        $('#' + id + ' .upgrade').hide();
    }
    else
    {
        $('#' + id + ' .upgradeBtn').show();
        $('#' + id + ' .upgrade').hide();
        MakeSelectOptions(
            $('#' + id + ' .upgrade select'),
            [ null ].concat(upgrades));
    }

    $('#' + id + ' .upgrade select').val('');
}


function ToggleUnitUpgradeVis(container)
{
    var upgElem = $('#' + container + ' .upgrade');
    var upgBtn = $('#' + container + ' .upgradeBtn');
    upgElem.toggle();
    var isVisible = upgElem.is(':visible');
    upgBtn.toggleClass('icon-chevron-right', !isVisible)
        .toggleClass('icon-chevron-down', isVisible);
    if (!isVisible)
    {
        upgBtn.data('clear')(upgBtn);
        UpdateForce();
    }
}

function UpdateUnitUpgradeVis(container, unit)
{
    var upgElem = $('#' + container + ' .upgrade');
    var upgBtn = $('#' + container + ' .upgradeBtn');
    if (unit && unit.upgrade) upgElem.show();
    var isVisible = upgElem.is(':visible');
    upgBtn.toggleClass('icon-chevron-right', !isVisible)
        .toggleClass('icon-chevron-down', isVisible);
}

//Function to update the overall force, including calculating the total number of points in the force.
function UpdateForce()
{
    Force.points = 0;

    Force.maxPlatoons = Math.ceil(Force.maxPoints / 150);
    Force.numPlatoons = 0;

    Force.maxHeroes = Math.ceil(Force.maxPoints / 150);
    Force.numHeroes = 0;

    Force.errors = [];
    Force.lastError = '';

    Force.numCapturedVehicles = 0;

    var heroes = [];

    var platoonNum = 0;
    var valid = true;

    //Iterate over all the platoons in the force.
    for (var p in Force.platoons)
    {
        var platoon = Force.platoons[p];
        platoon.points = 0;

        valid = platoon.definition.prevalidate(platoon);
        
        ++Force.numPlatoons;

        if (platoon.upgrade)
        {
            platoon.points += platoon.upgrade.points;
            if (platoon.upgrade.bonusHero)
            {
                ++Force.maxHeroes;
            }

            valid = platoon.upgrade.validate(platoon);

            if (platoon.upgrade.supports.length)
            {
                UpdatePointsTooltip(MakeBonusSupportId(p, 'upgrade', 0), platoon.bonusSupports['upgrade0'].Points(), valid);
            }
            else
            {
                UpdatePointsTooltip(MakeUpgradeId(p), platoon.upgrade.points, valid);
            }
        }

        var numSections = 0;
        if(platoon.definition.commissarsAllowed == true){
        	var maxCommissars = platoon.definition.numCommissars;
        } else {
        	var maxCommissars = 0;
        }

        for (var s in platoon.sections)
        {
            var unit = platoon.sections[s];

            valid = true;
            if (unit)
            {
                numSections += 1;
                if(platoon.definition.commissarsAllowed == true){
                	maxCommissars += unit.definition.numCommissars;
                } else {
                	maxCommissars = 0;
                }

                platoon.points += unit.Points();

                if (unit.definition.type == UnitType.Hero)
                {
                    ++Force.numHeroes;
                    if ($.inArray(unit.definition, heroes) >= 0)
                    {
                        ForceError("Duplicate Hero '" + unit.definition.name + "'.");
                        valid = false;
                    }
                    else
                    {
                        heroes.push(unit.definition);
                        for (hm in unit.definition.mutualExclusive)
                            heroes.push(unit.definition.mutualExclusive[hm]);
                    }
                }
            }

            UpdateUnitUpgradeVis(MakeSectionId(p, s), unit);

            UpdatePointsTooltip(MakeSectionId(p, s), unit ? unit.Points() : 0, valid);
        }

        //
        platoon.maxSupports = Math.floor(numSections / 2) + platoon.additionalSupports;
        platoon.numSupports = 0;
        for (s in platoon.supports)
        {
            unit = platoon.supports[s];

            ++platoon.numSupports;

            if (platoon.numSupports > platoon.maxSupports)
            {
                ForceError("Too many supports.");
                valid = false;
            }

            UpdateUnitUpgradeVis(MakeSupportId(p, s), unit);

            UpdatePointsTooltip(MakeSupportId(p, s), unit ? unit.Points() : 0, valid);
        }  //  .length lies

        $('#' + MakePlatoonId(p) + ' .addSupport button').prop("disabled",
            (platoon.definition.supports.length == 0 || platoon.numSupports >= platoon.maxSupports));

        $('#' + MakePlatoonId(p) + ' .numSupports').text(platoon.numSupports.toString());
        $('#' + MakePlatoonId(p) + ' .maxSupports').text(platoon.maxSupports.toString());

        for (s in platoon.bonusSupports)
        {
            unit = platoon.bonusSupports[s];
            UpdateUnitUpgradeVis(MakeBonusSupportId(p, s), unit);
        }

        platoon.maxCommissars = maxCommissars + (platoon.upgrade ? platoon.upgrade.numCommissars : 0);
        platoon.numCommissars = 0;
        var commissarsSeen = [];
        for (var c in platoon.commissars)
        {
            var commissar = platoon.commissars[c];
            ++platoon.numCommissars;

            if ($.inArray(commissar.definition, commissarsSeen) >= 0)
            {
                ForceError("Only one of each Commissar may be taken per platoon.");
                valid = false;
            }
            if (platoon.numCommissars > platoon.maxCommissars)
            {
                ForceError("Too many commissars.");
                valid = false;
            }

            commissarsSeen.push(commissar.definition);

            UpdatePointsTooltip(MakeCommissarId(p, c), commissar.Points(), valid);
        }

        $('#' + MakePlatoonId(p) + ' .addCommissar button').prop("disabled",
            (platoon.maxCommissars == 0 || platoon.numCommissars >= platoon.maxCommissars));

        $('#' + MakePlatoonId(p) + ' .numCommissars').text(platoon.numCommissars.toString());
        $('#' + MakePlatoonId(p) + ' .maxCommissars').text(platoon.maxCommissars.toString());

        for (s in platoon.supports)
        {
            var support = platoon.supports[s];
            platoon.points += support.Points();
        }

        for (s in platoon.bonusSupports)
        {
            support = platoon.bonusSupports[s];
            
	    //Handle captured vehicles:
		//Add extra point cost for captured vehicles
		//validate that there is only one captured vehicle in the force!
	    if(s.indexOf("captured") != -1 && support != null){
	   	Force.numCapturedVehicles++;
                platoon.points += support ? support.Points() + capturedExtraPointsCost : 0;
	    } else {
                platoon.points += support ? support.Points() : 0;
            }
	    
	    if(Force.numCapturedVehicles > 1){
		ForceError("Only 1 captured vehicle allowed.");
            	valid = false;
	    }

        }

        for (c in platoon.commissars)
        {
            commissar = platoon.commissars[c];
            platoon.points += commissar.Points();
        }

        valid = platoon.definition.validate(platoon);
        if (Force.numPlatoons > Force.maxPlatoons)
        {
            ForceError("Too many platoons.");
            valid = false;
        }

        UpdatePointsTooltip(MakePlatoonId(p) + ' .platoonpoints', platoon.points, valid);
        ++platoonNum;

        Force.points += platoon.points;
    }

    for (var h in Force.heroes)
    {
        var hero = Force.heroes[h];
        Force.points += hero.Points();
        ++Force.numHeroes;

        valid = true;
        if ($.inArray(hero.definition, heroes) >= 0)
        {
            ForceError("Duplicate Hero '" + hero.definition.name + '".');
            valid = false;
        }
        else
        {
            heroes.push(hero.definition);
            for (var hm in hero.definition.mutualExclusive)
                heroes.push(hero.definition.mutualExclusive[hm]);
        }

        if (Force.numHeroes > Force.maxHeroes)
        {
            //  Can override duplicate hero
            ForceError("Too many heroes.");
            valid = false;
        }

        UpdateUnitUpgradeVis(MakeHeroId(h), hero);

        UpdatePointsTooltip(MakeHeroId(h), hero.Points(), valid);
    }


    $('#addPlatoon').prop("disabled",
        (Force.numPlatoons >= Force.maxPlatoons
            && !$('#ignore-limits').is(':checked')));

    $('#numPlatoons').text(Force.numPlatoons.toString());
    $('#maxPlatoons').text(Force.maxPlatoons.toString());

    $('#addHero').prop("disabled",
        (Force.numHeroes >= Force.maxHeroes
            && !$('#ignore-limits').is(':checked')));

    $('#numHeroes').text(Force.numHeroes.toString());
    $('#maxHeroes').text(Force.maxHeroes.toString());

    var percentage = (Force.points / Force.maxPoints);

    var newBarClass = 'progress-success';
    if (percentage > 1.0)
    {
        newBarClass = 'progress-danger';
    }
    else if (Force.numHeroes > Force.maxHeroes
        || Force.numPlatoons > Force.maxPlatoons
        || Force.errors.length > 0)
    {
        newBarClass = 'progress-warning';
    }

    $('#forcePointsBar')
        .removeClass('progress-success progress-warning progress-danger')
        .addClass(newBarClass);

    $('#forcePointsBar .bar').width(Math.ceil(percentage * 100) + '%');


    //var newPointsLabel = 'badge-success';
    var sizeValid = Force.points <= Force.maxPoints;
    if (!sizeValid)
    {
        //newPointsLabel = 'badge-important';
        ForceError("Maximum Force size exceeded.");
    }

    $('#force-points')
        .text(Force.points.toString())
//        .removeClass('badge-success badge-important')
//        .addClass(newPointsLabel)
        .toggleClass('badge-success', sizeValid)
        .toggleClass('badge-important', !sizeValid);

    $('#force-tooltip').toggle(Force.errors.length > 0)
        .attr('data-html', true)
        .attr('data-content', Force.errors.join('<hr/>'))
        .popover();

    EncodeForceIntoURIParam();
}


function Export()
{
    //  Coming Soon
    var text = "Dust Warfare - Force Builder\n";

    if (Force.notes != "")
    {
        text += "\n" + Force.notes + "\n\n";
    }
    text += "Faction: " + Force.faction.name + " ( " + Force.points + " / " + Force.maxPoints + " )\n";

    if (Force.heroes.length)
    {
        text += "\n---  Heroes\n";
        for (var h in Force.heroes)
        {
            text += MakeNameWithPoints(Force.heroes[h].definition) + "\n";
        }
    }

    var sortedPlatoons = Force.platoons.slice(0);
    sortedPlatoons.sort(function(a,b) { return a.order - b.order; });

    for (var p in sortedPlatoons)
    {
        text += "\n";

        var platoon = sortedPlatoons[p];
        text += "---  " + (platoon.notes != "" ? '"' + platoon.notes + '" ' : "" ) + platoon.definition.name + " (" + platoon.points + ")\n";
	//Platoon Upgrades
        if (platoon.upgrade)
        {
            text += "Upgrade: " + MakeNameWithPoints(platoon.upgrade) + "\n";
        }
	//Sections
        for (var s = 0; s < NumSections; ++s)
        {
            var unit = platoon.sections[s];
            if (unit)
            {
                text += SectionName[s] + " Section: " + unit.definition.name + " (" + unit.definition.points + (unit.definition.type == UnitType.Hero ? ", Hero" : "") + ")\n";
                if (unit.upgrade) text += "    + " + unit.upgrade.name + " (" + unit.upgrade.points + ")\n";
            }
        }
	//Commissars
        for (var c in platoon.commissars)
        {
            text += "Commissar: " + MakeNameWithPoints(platoon.commissars[c].definition) + "\n";
        }
	//Supports
        for (s in platoon.supports)
        {
            unit = platoon.supports[s];
            text += "Support: " + unit.definition.name + " (" + unit.definition.points + ")\n";
            if (unit.upgrade) text += "    + " + unit.upgrade.name + " (" + unit.upgrade.points + ")\n";
        }
	//Bonus supports
        for (s in platoon.bonusSupports)
        {
            unit = platoon.bonusSupports[s];
            if (unit)
            {
		//Handle captured vehicles!
		if(s.indexOf("captured") != -1){ 
		    text += "Captured Vehicle: " + unit.definition.name + " (" + (unit.Points() + capturedExtraPointsCost) + ")\n";
		} else {
                    text += "Bonus Support: " + unit.definition.name + " (" + unit.Points() + ")\n";
		}

                if (unit.upgrade) text += "    + " + unit.upgrade.name + " (" + unit.upgrade.points + ")\n";
            }
        }
    }

    //Put in a link to the force
    text += "\n"    
    text += "Link to this force: " + document.URL + Force.LinkURI;

    if (navigator.appName != 'Microsoft Internet Explorer')
    {
        window.open('data:text/plain;charset=utf-8,' + encodeURIComponent(text), '_blank', 'location=no,toolbar=no');
    }
    else
    {
        var popup = window.open('', 'plain', '');
        popup.document.body.innerHTML = '<pre>' + text + '</pre>';
    }

    return text;
}


function Encoder(buf)
{
    this.buffer = buf || "";
    this.val = 0;
    this.bits = 0;
    this.chars = 0;
    this.codex = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    this.EncodeInt = function(i, bits)
    {
        if (i < 0) i = 0; // No negative numbers allowed

        this.val = this.val << bits;
        this.val |= i & ((1 << bits) - 1);
        this.bits += bits;

        while (this.bits >= 6)
        {
            var b = (this.val >> (this.bits - 6)) & 0x3F;
            this.buffer += this.codex.charAt((b + (this.buffer.length*3)) % this.codex.length);
            this.bits -= 6;
            this.val = this.val & ((1 << this.bits) - 1);   //  Optional: remove the bits we encoded
        }
    };

    this.EncodeBool = function(b) { this.EncodeInt(b ? 1 : 0, 1); return b; };

    this.EncodeString = function(str)
    {
        var offset = 0;

        str = encodeURI(str);
        var length = str.length;

        while (offset < length)
        {
            var c = str[offset];
            offset += 1;

            if ('%' !== c)
            {
                this.EncodeInt(c.charCodeAt(0), 8);
            }
            else
            {
                this.EncodeInt(parseInt(str.substr(offset, 2), 16), 8);
                offset += 2;
            }
        }

        this.EncodeInt(0, 8);
    };

    this.FinishEncode = function()
    {
        if (this.bits > 0)
        {
            this.val = this.val << (6 - this.bits);
            var b = (this.val >> (this.bits - 6)) & 0x3F;
            this.buffer += this.codex.charAt((b + (this.buffer.length*3)) % this.codex.length);

            this.buffer += this.codex.charAt(this.val & 0x3F);
            this.val = 0;
            this.bits = 0;
        }
    };

    this.DecodeInt = function(bits)
    {
        var val;
        while (this.bits < bits)
        {
            if (this.buffer.length == 0)
            {
                //  Error encountered, buffer ran out!
                this.bits = 0;
                return 0;
            }
            val = (this.codex.indexOf(this.buffer.charAt(0)) - (this.chars*3));
            while (val < 0) val += this.codex.length;

            this.val = this.val << 6;
            this.val = this.val | val;

            this.buffer = this.buffer.substr(1);

            this.chars += 1;
            this.bits += 6;
        }
        this.bits -= bits;
        val = (this.val >> this.bits) & ((1 << bits) - 1);
        this.val = this.val & ((1 << this.bits) - 1);   //  Optional: remove the bits we encoded
        return val;
    };

    this.DecodeBool = function() { return this.DecodeInt(1) == 1; };

    this.DecodeString = function()
    {
        var chars = '';

        while (this.Good())
        {
            var c = this.DecodeInt(8);
            var c2 = 0;
            var c3 = 0;

            if (c == 0)
                break;
            else if (c < 128)
            {
                chars += String.fromCharCode(c);
            }
            else if (c > 191 && c < 224)
            {
                c2 = this.DecodeInt(8);
                chars += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            }
            else
            {
                c2 = this.DecodeInt(8);
                c3 = this.DecodeInt(8);
                chars += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            }
        }

        return chars;
    };

    this.ToString = function()
    {
        this.FinishEncode();
        return this.buffer;
    };

    this.Good = function()
    {
        return this.buffer.length > 0 || this.bits > 0;
    };
}


function EncodeForceIntoURIParam()
{
    //  Version
    //  1 = First implementation
    //  2 = New Format
    //  3 = Merge upgraded unit versions to be upgrades of original units

    var encoder = new Encoder();
    encoder.EncodeInt(3, 4);  //  Version
    encoder.EncodeInt(Force.faction.id, 3);
    encoder.EncodeInt(Force.maxPoints, 12);

    if (encoder.EncodeBool(Force.notes.length > 0))
    {
        encoder.EncodeString(Force.notes);
    }

    //  Can't encode Force.numHeroes because that includes heroes in Platoons
    var numHeroes = 0;
    for (var h in Force.heroes) { ++numHeroes; }
    encoder.EncodeInt(numHeroes, 5);
    for (h in Force.heroes)
    {
        encoder.EncodeString(Force.heroes[h].definition.id);
    }

    encoder.EncodeInt(Force.numPlatoons, 5);

    var sortedPlatoons = Force.platoons.slice(0);
    sortedPlatoons.sort(function(a,b) { return a.order - b.order; });
    for (var p in sortedPlatoons)
    {
        var platoon = sortedPlatoons[p];

        encoder.EncodeString(platoon.definition.id);

        if (encoder.EncodeBool(platoon.notes.length > 0))
        {
            encoder.EncodeString(platoon.notes);
        }

        if (encoder.EncodeBool(platoon.upgrade != null))
        {
            encoder.EncodeString(platoon.upgrade.id);
        }

        for (var s = 0; s < NumSections; ++s)
        {
            var unit = platoon.sections[s];
            if (encoder.EncodeBool(unit != null))
            {
                encoder.EncodeString(unit.definition.id);
                if (encoder.EncodeBool(unit.upgrade != null))
                {
                    encoder.EncodeString(unit.upgrade.id);
                }
            }
        }

        if (Force.faction == ssuFaction)
        {
            encoder.EncodeInt(platoon.numCommissars, 3);
            for (var c in platoon.commissars)
            {
                encoder.EncodeString(platoon.commissars[c].definition.id);
            }
        }

        encoder.EncodeInt(platoon.numSupports, 3);
        for (s in platoon.supports)
        {
            unit = platoon.supports[s];
            encoder.EncodeString(unit.definition.id);
            if (encoder.EncodeBool(unit.upgrade != null))
            {
                encoder.EncodeString(unit.upgrade.id);
            }
        }

        //  Bonus Supports
        if (platoon.upgrade && platoon.upgrade.supports)
        {
            unit = platoon.bonusSupports['upgrade0'];
            if (unit)
            {
                encoder.EncodeBool(true);
                encoder.EncodeInt(0, 2);
                encoder.EncodeString(unit.definition.id);
                if (encoder.EncodeBool(unit.upgrade != null))
                {
                    encoder.EncodeString(unit.upgrade.id);
                }
            }
        }

        for (s = 0; s < NumSections; ++s)
        {
            unit = platoon.sections[s];
            if (unit && unit.definition.supports.length)
            {
                unit = platoon.bonusSupports['section' + s];
                if (unit)
                {
                    encoder.EncodeBool(true);
                    encoder.EncodeInt(1, 2);
                    encoder.EncodeInt(s, 3);
                    encoder.EncodeString(unit.definition.id);
                    if (encoder.EncodeBool(unit.upgrade != null))
                    {
                        encoder.EncodeString(unit.upgrade.id);
                    }
                }
            }
        }

        //  Captured Vehicles
        for (var cv in platoon.bonusSupports)
        {
            unit = platoon.bonusSupports[cv];
            
	    if(cv.indexOf("captured") != -1 && unit != null){  

		//Get the numerical index of the captured element
		var index = cv.substr(cv.indexOf("captured") + "captured".length, cv.length);
     
		encoder.EncodeBool(true);
                encoder.EncodeInt(3, 2);
	        encoder.EncodeString(index);
                encoder.EncodeString(unit.definition.id);
                if (encoder.EncodeBool(unit.upgrade != null))
                {
                    encoder.EncodeString(unit.upgrade.id);
                }
            }
        }

        for (s in platoon.definition.additionalSupports)
        {
            unit = platoon.bonusSupports['additional' + s];
            if (unit)
            {
                encoder.EncodeBool(true);
                encoder.EncodeInt(2, 2);
                encoder.EncodeString(s);
                encoder.EncodeString(unit.definition.id);
                if (encoder.EncodeBool(unit.upgrade != null))
                {
                    encoder.EncodeString(unit.upgrade.id);
                }
            }
        }

/*
        //  Captured Vehicles
        for (var cv in platoon.bonusSupports)
        {
            unit = platoon.bonusSupports[cv];
            
	    if(cv.indexOf("captured") != -1 && unit != null){       
		encoder.EncodeBool(true);
                encoder.EncodeInt(3, 2);
	        encoder.EncodeString(cv);
                encoder.EncodeString(unit.definition.id);
                if (encoder.EncodeBool(unit.upgrade != null))
                {
                    encoder.EncodeString(unit.upgrade.id);
                }
            }
        }
*/

        encoder.EncodeBool(false);
    }

    var url = '#' + encoder.ToString();
    $('#link').attr('href', url);

    //Save the URI
    Force.LinkURI = url;

}


var DeprecatedUnitsToUpgradedUnits = {
    'CmR+': [ 'CmbR', 'CmbR+' ],
    'M3D+': [ 'M3D', 'M3Am' ],
    'M3E+': [ 'M3E', 'M3Am' ],
    'BGr+': [ 'BGrn', 'BGrn+' ],
    'MPW3A+': [ 'MPW3A', 'MPW3A+' ]
};

function DecodeForceFromURIParam(hash)
{
    //  Version = 3
    var encoder = new Encoder(hash.substr(1));
    var version = encoder.DecodeInt(4);  //  Version

    SetFaction(encoder.DecodeInt(3));
    SetForceSize(encoder.DecodeInt(12));

    if (version == 1)
    {
        //  Redirect to Legacy
        //alert('Due to significant changes in Dust Warfare Force Builder to support Campaign Book: Hades, Force Links saved before December 17th do not currently work, but this may change!');
        window.open('convert.html?' + hash, '_self');
        return false;
    }

    if (encoder.DecodeBool())
    {
        Force.notes = encoder.DecodeString();
        $('#force .notes').val(Force.notes);
    }

    var numHeroes = encoder.DecodeInt(5);

    $('#heroes').empty();
    Force.heroes = [];
    Force.numHeroes = 0;

    $('#platoons').empty();
    Force.platoons = [];
    Force.numPlatoons = 0;

    for (var h = 0; h < numHeroes; ++h)
    {
        AddHero(encoder.DecodeString());
    }

    var numPlatoons = encoder.DecodeInt(5);

    for (var p = 0; p < numPlatoons; ++p)
    {
        var platoonId = AddPlatoon(encoder.DecodeString());
        var platoon = Force.platoons[platoonId];

        if (encoder.DecodeBool())
        {
            platoon.notes = encoder.DecodeString();
            $('#' + MakePlatoonId(platoonId) + ' .notes').val(platoon.notes);
        }

        if (encoder.DecodeBool())
        {
            SetPlatoonUpgrade(platoonId, encoder.DecodeString());
        }

        for (var s = 0; s < NumSections; ++s)
        {
            if (encoder.DecodeBool())
            {
                var unitId = encoder.DecodeString();
                var upgradeId = encoder.DecodeBool() ? encoder.DecodeString() : null;

                if (version == 2 &&
                    unitId in DeprecatedUnitsToUpgradedUnits)
                {
                    upgradeId = DeprecatedUnitsToUpgradedUnits[unitId][1];
                    unitId = DeprecatedUnitsToUpgradedUnits[unitId][0];
                }

                SetSection(platoonId, s, unitId);
                if (upgradeId)
                {
                    SetSectionUpgrade(platoonId, s, upgradeId);
                }
            }
        }

        if (Force.faction == ssuFaction)
        {
            var numCommissars = encoder.DecodeInt(3);
            for (var c = 0; c < numCommissars; ++c)
            {
                AddCommissar(platoonId, encoder.DecodeString());
            }
        }

        var numSupports = encoder.DecodeInt(3);
        for (s = 0; s < numSupports; ++s)
        {
            unitId = encoder.DecodeString();
            upgradeId = encoder.DecodeBool() ? encoder.DecodeString() : null;

            if (version == 2 &&
                unitId in DeprecatedUnitsToUpgradedUnits)
            {
                upgradeId = DeprecatedUnitsToUpgradedUnits[unitId][1];
                unitId = DeprecatedUnitsToUpgradedUnits[unitId][0];
            }

            var supportId = AddSupport(platoonId, unitId);
            if (upgradeId)
            {
                SetSupportUpgrade(platoonId, supportId, upgradeId);
            }
        }

        while (encoder.DecodeBool())
        {
            var bonusSupportType = encoder.DecodeInt(2);
            var type = null;
            supportId = 0;

            if (bonusSupportType == 0)
            {
                type = 'upgrade';
            }
            else if (bonusSupportType == 1)
            {
                type = 'section';
                supportId = encoder.DecodeInt(3);
            }
            else if (bonusSupportType == 2)
            {
                type = 'additional';
                supportId = encoder.DecodeString();
            }
            else if (bonusSupportType == 3)
            {
                type = 'captured';
                supportId = encoder.DecodeString();
            }
            else
            {
                continue;
            }

            unitId = encoder.DecodeString();
            upgradeId = encoder.DecodeBool() ? encoder.DecodeString() : null;

            if (version == 2 &&
                unitId in DeprecatedUnitsToUpgradedUnits)
            {
                upgradeId = DeprecatedUnitsToUpgradedUnits[unitId][1];
                unitId = DeprecatedUnitsToUpgradedUnits[unitId][0];
            }

            SetBonusSupport(platoonId, type, supportId, unitId);

            if (upgradeId)
            {
                SetBonusSupportUpgrade(platoonId, type, supportId, upgradeId);
            }
        }
    }

    return true;
}

function SetCookie(cookieName, value, expDays)
{
    var expDate = new Date();
    expDate.setDate(expDate.getDate() + expDays);
    var cookieValue = encodeURIComponent(value) + ((expDays==null) ? "" : "; expires="+expDate.toUTCString());
    document.cookie = cookieName + "=" + cookieValue;
}

function GetCookie(cookieName)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x == cookieName)
        {
            return decodeURIComponent(y);
        }
    }
    return "";
}

function Load()
{
    $('#force-size').val(Force.maxPoints);

    if (window.location.hash)
    {
        DecodeForceFromURIParam(window.location.hash);
    }
    else
    {
        var faveFaction = GetCookie('faction');
        var faction = faveFaction ? parseInt(faveFaction) : Faction.Allies;
        if (faction < 0 || faction > maxFaction) faction = Faction.Allies;

        SetFaction(faction);

        var forceSize = GetCookie('size');
        forceSize = forceSize ? parseInt(forceSize) : 0;
        if (forceSize != 0)
        {
            SetForceSize(forceSize);
        }
    }
    UpdateForce();

    $('#revision').text(Revision.toString());
}
