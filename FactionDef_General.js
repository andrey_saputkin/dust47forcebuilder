//  GENERAL
//   - Unit Upgrades
var upgUnitBattlefieldEngineers = new UpgradeDefinition('BEng', 'Battlefield Engineers', 2);
var upgUnitRatFighters          = new UpgradeDefinition('RatF', 'Rat Fighters', 1);
var upgUnitPinpointObservation  = new UpgradeDefinition('PnOb', 'Pinpoint Observation', 4);

var upgUnitGeneral = [
    upgUnitBattlefieldEngineers,
    upgUnitRatFighters,
    upgUnitPinpointObservation
];

//- Strongpoints
var vhStrongpoint = new UnitDefinition('Stpt', 'Strongpoint', 15, UnitType.Vehicle);
var vhBunker = new UnitDefinition('Bnkr', 'Bunker', 25, UnitType.Vehicle);

/*
//Generic Platoon Definition
var genericPlatoon = new PlatoonDefinition('GPlt', 'Generic Platoon');
genericPlatoon.sections = [
    [  ],
    [  ],
    [ null,  ],
    [ null,  ],
    [ null,  ]
];
axisSturmgrenadierePlatoon.supports = [
    
];
*/
