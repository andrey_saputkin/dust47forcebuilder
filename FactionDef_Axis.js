//  AXIS
var sqAxisKommandotrupp     = new UnitDefinition('Ktrp', 'Kommandotrupp', 25);
var sqAxisHeavyKommandotrupp= new UnitDefinition('HKtp', 'Heavy Kommandotrupp', 35);
var sqAxisSturmpioniere     = new UnitDefinition('Strm', 'Sturmpioniere', 25);
var sqAxisNDAKCommand	    = new UnitDefinition('FxCb', '"Fox Cubs" NDAK Command Squad', 15);

var sqAxisBattleGrenadiers  = new UnitDefinition('BGrn', 'Battle Grenadiers', 17);
sqAxisBattleGrenadiers.upgrades = [ new UpgradeDefinition('BGrn+', 'Replace one StG47 with Panzerschreck', 3) ];
var sqAxisLaserGrenadiers   = new UnitDefinition('LGrn', 'Laser Grenadiers', 21);
var sqAxisReconGrenadiers   = new UnitDefinition('RGrn', 'Recon Grenadiers', 17);
var sqAxisZombies           = new UnitDefinition('Zmbs', 'Axis Zombies', 19);

var sqAxisSniperGrenadierTeam   = new UnitDefinition('SnpG', 'Sniper Grenadier Team', 12);
var sqAxisBeobachterTeam    = new UnitDefinition('Beob', 'Beobachter Team', 8);

var sqAxisNDAKTankHunter    = new UnitDefinition('NDTH', '"African Lions" NDAK Tank Hunter Squad', 14);
var sqAxisNDAKBattleGren    = new UnitDefinition('NDBG', '"Sand Vipers" NDAK Battle Grenadiers Squad', 15);
var sqAxisNDAKSecurity 	    = new UnitDefinition('NDSG', '"Tomb Cleaners" NDAK Security Grenadiers Squad', 15);
var sqAxisNDAKReconGren	    = new UnitDefinition('NDRG', '"Desert Scorpions" NDAK Recon Grenadiers Squad', 16);
var sqAxisNDAKObserver      = new UnitDefinition('NDOb', '"Desert Eagles" NDAK Observer Squad', 7);
var sqAxisNDAKMortar	    = new UnitDefinition('NDMr', '"Sandstorm" NDAK Mortar Team', 16);
var sqAxisNDAKMortarAndObservers = new UnitDefinition('NDMO', '"Sandstorm and Desert Eagles" NDAK Mortar Team with Observers', 23);
//TODO - This should be changed to support seperate items for each to support upgrades on each.

var sqAxisHeavyFlameGren    = new UnitDefinition('HvFm', '"Desert Sun" Heavy Flame Grenadier Squad', 30);

var sqAxisGorillas          = new UnitDefinition('Grla', 'Axis Gorillas', 24);
var sqAxisHeavyReconGrenadiers  = new UnitDefinition('HRGr', 'Heavy Recon Grenadiers', 30);
var sqAxisHeavyLaserGrenadiers  = new UnitDefinition('HLGr', 'Heavy Laser Grenadiers', 35);
var sqAxisHeavyFlakGrenadiers   = new UnitDefinition('HFGr', 'Heavy Flak Grenadiers', 26);

var sqAxisJagdgrenadiere    = new UnitDefinition('Jgdg', 'Jagdgrenadiere', 23);
var sqAxisLaserJagdgrenadiere   = new UnitDefinition('LJgd', 'Laser-Jagdgrenadiere', 25);

var sqAxisUbertotenAssaultSquad = new UnitDefinition('UbAS', '"Braineaters" Übertoten Assault Squad', 24);
var sqAxisUbertotenSuicideSquad = new UnitDefinition('UbSS', '"Heilige Tod" Übertoten Suicide Squad', 22);

var sqAxisBlutkreuzPionereSquad = new UnitDefinition('BKKPS', '"Sturmaffe" Blutkreuz Korps Kampfaffen Pionere Squad', 40);

//Vehicles
var vhAxisLPWIAHeinrich     = new UnitDefinition('LPW1A',  'LPW I-A "Heinrich"', 20, UnitType.Vehicle);
var vhAxisLPWIBHermann      = new UnitDefinition('LPW1B',  'LPW I-B "Hermann"', 20, UnitType.Vehicle);
var vhAxisLPWICHans         = new UnitDefinition('LPW1C',  'LPW I-C "Hans"', 25, UnitType.Vehicle);
var vhAxisLPWIEHeinrichTrop = new UnitDefinition('LPW1E',  'LPW I-E "Heinrich (Trop)"', 22, UnitType.Vehicle);
var vhAxisLPWIDHermannTrop  = new UnitDefinition('LPW1D',  'LPW I-D "Hermann (Trop)"', 19, UnitType.Vehicle);
var vhAxisAnyLPW = [ vhAxisLPWIAHeinrich, vhAxisLPWIBHermann, vhAxisLPWICHans, vhAxisLPWIEHeinrichTrop, vhAxisLPWIDHermannTrop];

var vhAxisTPWVICPrinzluther = new UnitDefinition('TPW6C',  'TPW VI-C "Prinzluther"', 38, UnitType.Vehicle);
var vhAxisTPWVIDSturmprinz  = new UnitDefinition('TPW6D',  'TPW VI-D "Sturmprinz"', 45, UnitType.Vehicle);
var vhAxisTPWVIGStummel     = new UnitDefinition('TPW6G',  'TPW VI-G "Stummel"', 45, UnitType.Vehicle);
var vhAxisAnyArmoredTransport = [ vhAxisTPWVICPrinzluther, vhAxisTPWVIDSturmprinz, vhAxisTPWVIGStummel ];

var vhAxisMPWIIALuther      = new UnitDefinition('MPW2A',  'MPW II-A "Luther"', 30, UnitType.Vehicle);
var vhAxisMPWIIBLudwig      = new UnitDefinition('MPW2B',  'MPW II-B "Ludwig"', 40, UnitType.Vehicle);
var vhAxisMPWIIDLothar      = new UnitDefinition('MPW2D',  'MPW II-D "Lothar"', 40, UnitType.Vehicle);
var vhAxisMPWIIELoth        = new UnitDefinition('MPW2E',  'MPW II-E "Loth"', 25, UnitType.Vehicle);
var vhAxisMPWIICLeopold     = new UnitDefinition('MPW2C',  'MPW II-C "Leopold"', 35, UnitType.Vehicle);
var vhAxisMPWIIFSturmLeopold = new UnitDefinition('MPW2F',  'MPW II-F "Sturmleopold"', 45, UnitType.Vehicle);
var vhAxisMPWIIFLothTrop    = new UnitDefinition('MPW2FLT',  'MPW II-F "Loth (Trop)"', 20, UnitType.Vehicle);

var vhAxisHPWIVJagdluther   = new UnitDefinition('HPW4',  'HPW IV "Jagdluther"', 55, UnitType.Vehicle);
var vhAxisHPWIVEJagdwotan   = new UnitDefinition('HPW4E',  'HPW IV-E "Jagdwotan"', 50, UnitType.Vehicle);
var vhAxisAnyMPWIV = [ vhAxisHPWIVJagdluther, vhAxisHPWIVEJagdwotan ];

var vhAxisAnyMPWII = [ vhAxisMPWIIALuther, vhAxisMPWIIBLudwig, vhAxisMPWIIDLothar, vhAxisMPWIIELoth, 
                       vhAxisMPWIICLeopold, vhAxisMPWIIFSturmLeopold, vhAxisMPWIIFLothTrop, 
                       vhAxisHPWIVJagdluther, vhAxisHPWIVEJagdwotan, vhAxisTPWVIGStummel ];

var vhAxisKV47ROtto 	   = new UnitDefinition('AKV47',  'KV-47(R) "Otto"', 30, UnitType.Vehicle);

var vhAxisMPWIIIAWotan      = new UnitDefinition('MPW3A',  'MPW III-A "Wotan"', 50, UnitType.Vehicle);
vhAxisMPWIIIAWotan.upgrades = [ new UpgradeDefinition('MPW3A+', 'Wotan Upgrade to Vehicle 6', 5) ];
var vhAxisMPWIIIDFlammLuther= new UnitDefinition('MPW3D',  'MPW III-D "Flamm-Luther"', 40, UnitType.Vehicle);
var vhAxisAnyMPWIII = [ vhAxisMPWIIIAWotan, vhAxisMPWIIIDFlammLuther ];

var vhAxisHPWVIAKonigsluther= new UnitDefinition('HPW6A',  'HPW VI-A "Königsluther"', 95, UnitType.Vehicle);
var vhAxisHPWVIBSturmkonig  = new UnitDefinition('HPW6B',  'HPW VI-B "Sturmkönig"', 90, UnitType.Vehicle);
var vhAxisHPWVIEKonigslothar= new UnitDefinition('HPW6E',  'HPW VI-E "Königslothar"', 77, UnitType.Vehicle);
var vhAxisAnyHPWVI = [ vhAxisHPWVIAKonigsluther, vhAxisHPWVIBSturmkonig, vhAxisHPWVIEKonigslothar ];

var vhAxisHO347III          = new UnitDefinition('HO3473', 'Horten Ho-347.III "Fledermaus III"', 45, UnitType.Vehicle);
var vhAxisHO347IV           = new UnitDefinition('HO3474', 'Horten Ho-347.IV "Fledermaus IV"', 45, UnitType.Vehicle);
var vhAxisAnyHO347 = [ vhAxisHO347III, vhAxisHO347IV ];

var vhAxisHO357V            = new UnitDefinition('HO3575', 'Horten Ho-357 Fledermaus V "Der Adler"', 50, UnitType.Vehicle);
var vhAxisHO357VII          = new UnitDefinition('HO3577', 'Horten Ho-357 Fledermaus VII "Der Blitz"', 50, UnitType.Vehicle);
var vhAxisAnyHO357 = [ vhAxisHO357V, vhAxisHO357VII ];

var vhAxisFestungLaserKanoneAusf47 = new UnitDefinition('FLK47', 'Festung mit Laser Kanone Ausf. 47', 25, UnitType.Vehicle);
var upgVhFLKA47ZweiLaserKanone  = new UpgradeDefinition('F47Z', 'Zwei Laser Kanone', 10);
var upgVhFLKA47Bunker       = new UpgradeDefinition('F47B', 'Bunker', 15);
vhAxisFestungLaserKanoneAusf47.upgrades = [
    upgVhFLKA47ZweiLaserKanone, upgVhFLKA47Bunker
];

//Capturable vehicles definition
var vhAxisCapturable = [ vhAxisLPWIAHeinrich, vhAxisLPWICHans, vhAxisLPWIEHeinrichTrop, vhAxisMPWIIALuther, vhAxisMPWIIBLudwig, vhAxisMPWIIDLothar, vhAxisMPWIIELoth, vhAxisMPWIICLeopold, vhAxisMPWIIFSturmLeopold, vhAxisMPWIIFLothTrop, vhAxisTPWVICPrinzluther, vhAxisTPWVIDSturmprinz, vhAxisTPWVIGStummel, vhAxisHPWIVJagdluther, vhAxisMPWIIIDFlammLuther, vhAxisHPWVIAKonigsluther, vhAxisHPWVIBSturmkonig, vhAxisHPWVIEKonigslothar ];

//Axis Heroes
var hSigridVonThaler        = new UnitDefinition('SgVT', 'Sigrid Von Thaler', 20, UnitType.Hero);
var hLara                   = new UnitDefinition('Lara', 'Lara', 29, UnitType.Hero);
var hGrenadierX             = new UnitDefinition('GrnX', 'Grenadier X', 21, UnitType.Hero);
var hMarkus                 = new UnitDefinition('Mrks', 'Markus', 24, UnitType.Hero);
var hManfred                = new UnitDefinition('Mnfd', 'Manfred', 15, UnitType.Hero);
var hAngela                 = new UnitDefinition('Angl', 'Angela', 30, UnitType.Hero);
var hStefan                 = new UnitDefinition('Stfn', 'Stefan', 18, UnitType.Hero);
var hTotenmeister           = new UnitDefinition('Totn', 'Totenmeister', 28, UnitType.Hero);
var hPanzerprinz            = new UnitDefinition('Pnzp', 'Panzerprinz', 24, UnitType.Hero);
var hPanzerprinzAndJagdluther = new UnitDefinition('PzpJ', 'Panzerprinz with Jagdluther', 79, UnitType.Hero);
var hGeneralmajorSigridVonThaler = new UnitDefinition('GSvT', 'Generalmajor Sigrid von Thaler', 30, UnitType.Hero);
hGeneralmajorSigridVonThaler.mutualExclusive = [ hSigridVonThaler ];
hSigridVonThaler.mutualExclusive = [ hGeneralmajorSigridVonThaler ];
var hHauptmanKlausVonRichthofen = new UnitDefinition('HKvR', 'Hauptman Klaus von Richthofen', 28, UnitType.Hero);
var hDesertFox              = new UnitDefinition('DstFx', 'The Desert Fox', 15, UnitType.Hero);
var hTinaAndHyne			= new UnitDefinition('TinaH', 'Tina and Hyne', 17, UnitType.Hero);
var hRolf 					= new UnitDefinition('Rolf', 'Rolf', 12, UnitType.Hero);
var hAngela_Spy             = new UnitDefinition('AngS', 'Angela - Bleutkreutz Spy', 11, UnitType.Hero);

//Leader Definitions
//??? - Markus has leader, but specific to his platoon.  Assuming not intended for this...
var hAnyAxis_Leaders 		= [ hSigridVonThaler, hLara, hManfred, hStefan, hPanzerprinz, hPanzerprinzAndJagdluther, hGeneralmajorSigridVonThaler, hDesertFox, hTinaAndHyne, hRolf ];

//Default Section 
var anyAxis_2ndSectionUnits = [  ].concat(anyMerc_2ndSectionUnits); 
var anyAxis_3rdSectionUnits = [  ].concat(anyMerc_3rdSectionUnits); 
var anyAxis_4thSectionUnits = [  ].concat(anyMerc_4thSectionUnits); 
var anyAxis_SupportSectionUnits = [ sqAxisNDAKObserver, sqAxisNDAKMortar, sqAxisNDAKMortarAndObservers ].concat(anyMerc_SupportSectionUnits);

var upgAxisDefenses             = new UpgradeDefinition('Dfns', 'Defenses', 5);
var upgAxisExtraPanzerSupport   = new UpgradeDefinition('XPnz', 'Extra Panzer Support', 10);
upgAxisExtraPanzerSupport.supports = [
    vhAxisLPWIAHeinrich, vhAxisLPWIEHeinrichTrop, vhAxisLPWIBHermann, vhAxisLPWIDHermannTrop, vhAxisLPWICHans,
    vhAxisMPWIIALuther, vhAxisMPWIIBLudwig, vhAxisMPWIIDLothar, vhAxisMPWIIELoth, vhAxisMPWIIFLothTrop,
    vhAxisMPWIIIAWotan, vhAxisMPWIIIDFlammLuther,
    vhStrongpoint, vhBunker
];
//This variable sets a boundary between the light walkers in the variable above and the medium walkers.  Used to validate the correct use of the upgrade.
var upgAxisExtraPanzerSupport_NumLightWalkers = 5;
upgAxisExtraPanzerSupport.validate = function (platoon)
{
    //  Limit once / force
    var numExtraPanzer = 0;
    for (var p in Force.platoons)
    {
        if (Force.platoons[p].upgrade == upgAxisExtraPanzerSupport)
        {
            if (++numExtraPanzer > 1)
            {
                ForceError("The 'Extra Panzer Support' Platoon Upgrade may only be taken once.");
                return false;
            }
            if (Force.platoons[p] == platoon)
                break;
        }
    }

    //  Size of walker limited by number of sections
    //  NOTE: First 3 walkers are always allowed, the rest are always allowed if 4 sections are filled
    var numSections = 0;
    for (var s in platoon.sections) {
        if (platoon.sections[s]) ++numSections;
    }
    if (numSections > 3) return true;

    var support = platoon.bonusSupports['upgrade0'];
    if ($.inArray(support.definition, this.supports) >= upgAxisExtraPanzerSupport_NumLightWalkers)
    {
        ForceError("'Extra Panzer Support' - Medium Walkers require you to take at least 4 Sections.");
        return false;
    }
    return true;
};
upgAxisExtraPanzerSupport.computeSupports = function (platoon) {
    var supports = [];
    for (var s in this.supports)
    {
        var support = this.supports[s];
        if ($.inArray(support, platoon.definition.supports) >= 0)
        {
            supports.push(support);
        }
    }
    return supports;
};
var upgAxisImplacable       = new UpgradeDefinition('Impl', 'Implacable', 15);
var upgAxisImprovedCommand  = new UpgradeDefinition('XICm', 'Improved Command', 5);
upgAxisImprovedCommand.prevalidate = function (platoon)
{
    return platoon.definition == axisSturmgrenadierePlatoon
        || platoon.definition == axisBlutkreuzPlatoon;
};
upgAxisImprovedCommand.validate = upgAlliesImprovedCommand.validate;
var upgAxisLightningWar         = new UpgradeDefinition('LtnW', 'Lightning War', 10);
var upgAxisNebelwerferBarrage   = new UpgradeDefinition('NBar', 'Nebelwerfer Barrage', 20);
var upgAxisSchwerArmor          = new UpgradeDefinition('SchA', 'Schwer Armor', 10);
upgAxisSchwerArmor.validate = function (platoon)
{
    if (platoon.sections[0].definition.type != UnitType.Hero)
    {
        ForceError("'" + this.name + "' is only useful if the Platoon is led by a Hero.");
        return false;
    }
    return true;
};
var upgAxisPotatoMashers        = new UpgradeDefinition('PMsh', 'Potato Mashers', 10);
var upgAxisPanzerPioniere       = new UpgradeDefinition('PzPn', 'Panzerpioniere', 5);
var upgAxisDerSchwarm           = new UpgradeDefinition('DSwm', 'Der Schwarm', 20);
var upgAxisSecondGenerationSerum = new UpgradeDefinition('2Srm', 'Second-Generation Serum', 2);
upgAxisSecondGenerationSerum.prevalidate = function (platoon)
{
    return platoon.definition == axisBlutkreuzPlatoon;
};

var upgUnitAxisGrenades         = new UpgradeDefinition('Grds', 'Grenades', 1);
var upgUnitAxisHighVisibilityLasers     = new UpgradeDefinition('HVLs', 'High Visibility Lasers', 2);
var upgUnitAxisOverchargedPowerCells    = new UpgradeDefinition('OcPC', 'Overcharged Power Cells', 3);
var upgUnitAxisAirEnhancedFliegerfaust  = new UpgradeDefinition('AirF', 'Air Enhanced Fliegerfaust', 2);

//Define Command Squad Additional Supports
sqAxisKommandotrupp.supports = [ null, sqAxisBeobachterTeam, sqAxisSniperGrenadierTeam, sqAxisJagdgrenadiere, vhAxisTPWVICPrinzluther, sqAxisNDAKObserver ];
sqAxisHeavyKommandotrupp.supports = [ null, sqAxisBeobachterTeam, sqAxisSniperGrenadierTeam, sqAxisJagdgrenadiere, vhAxisTPWVICPrinzluther, sqAxisNDAKObserver ];
sqAxisSturmpioniere.supports = [ null, sqAxisBeobachterTeam, sqAxisSniperGrenadierTeam, sqAxisJagdgrenadiere, vhAxisTPWVICPrinzluther, sqAxisNDAKObserver ];

//Sturmgrenadiere Platoon Definition
var axisSturmgrenadierePlatoon = new PlatoonDefinition('XStm', 'Sturmgrenadiere Platoon');
axisSturmgrenadierePlatoon.sections = [
    [ sqAxisKommandotrupp, hManfred, hStefan, hPanzerprinz, hPanzerprinzAndJagdluther, hGeneralmajorSigridVonThaler, hDesertFox, hTinaAndHyne ],
    [ sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisLaserGrenadiers, sqAxisNDAKSecurity, sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisHeavyFlakGrenadiers, sqAxisHeavyFlameGren ],
    [ null, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisLaserGrenadiers, sqAxisNDAKSecurity, sqAxisGorillas ].concat(anyAxis_2ndSectionUnits),
    [ null, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisHeavyReconGrenadiers, sqAxisHeavyLaserGrenadiers, sqAxisLaserJagdgrenadiere ].concat(anyAxis_3rdSectionUnits),
    [ null, sqAxisLaserGrenadiers, sqAxisNDAKSecurity, sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisHeavyReconGrenadiers, sqAxisHeavyLaserGrenadiers, sqAxisLaserJagdgrenadiere ].concat(anyAxis_4thSectionUnits)
];
axisSturmgrenadierePlatoon.supports = [
    sqAxisBeobachterTeam, sqAxisSniperGrenadierTeam,
    sqAxisJagdgrenadiere,
    sqAxisBlutkreuzPionereSquad,
    vhAxisMPWIIIAWotan, vhAxisMPWIIIDFlammLuther,
    vhAxisTPWVICPrinzluther, vhAxisTPWVIDSturmprinz,
    vhAxisHO347III, vhAxisHO347IV,
    vhAxisHO357V, vhAxisHO357VII,
    vhStrongpoint, vhBunker, vhAxisFestungLaserKanoneAusf47
].concat(vhAxisAnyLPW, vhAxisAnyMPWII, vhAxisAnyHPWVI, anyAxis_SupportSectionUnits);

//Schwer Platoon Definition
var axisSchwerPlatoon = new PlatoonDefinition('XSch', 'Schwer Platoon');
axisSchwerPlatoon.sections = [
    [ sqAxisHeavyKommandotrupp, hLara, hGeneralmajorSigridVonThaler, hDesertFox, hTinaAndHyne ],
    [ sqAxisGorillas, sqAxisHeavyReconGrenadiers, sqAxisHeavyFlakGrenadiers, sqAxisHeavyFlameGren, sqAxisHeavyLaserGrenadiers, sqAxisLaserJagdgrenadiere ],
    [ null, sqAxisHeavyReconGrenadiers, sqAxisHeavyFlakGrenadiers, sqAxisHeavyFlameGren, sqAxisHeavyLaserGrenadiers, sqAxisLaserJagdgrenadiere, sqAxisBlutkreuzPionereSquad ].concat(anyAxis_2ndSectionUnits),
    [ null, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisHeavyReconGrenadiers ].concat(anyAxis_3rdSectionUnits),
    [ null, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisHeavyReconGrenadiers, sqAxisHeavyLaserGrenadiers, sqAxisLaserJagdgrenadiere ].concat(anyAxis_4thSectionUnits)
];
axisSchwerPlatoon.supports = [
    sqAxisBeobachterTeam, sqAxisSniperGrenadierTeam,
    sqAxisJagdgrenadiere,
    sqAxisBlutkreuzPionereSquad,
    vhAxisMPWIIALuther, vhAxisMPWIIBLudwig, vhAxisMPWIIELoth, vhAxisMPWIIFLothTrop,
    vhAxisHO347III, vhAxisHO347IV,
    vhAxisHO357V, vhAxisHO357VII,
    vhStrongpoint, vhBunker, vhAxisFestungLaserKanoneAusf47
].concat(vhAxisAnyHPWVI, anyAxis_SupportSectionUnits);

//Blutkreuz Platoon Definition
var axisBlutkreuzPlatoon = new PlatoonDefinition('XBlt', 'Blutkreuz Platoon');
axisBlutkreuzPlatoon.sections = [
    [ sqAxisSturmpioniere, hSigridVonThaler, hManfred, hGeneralmajorSigridVonThaler, hDesertFox, hTinaAndHyne ],
    [ sqAxisZombies, sqAxisHeavyLaserGrenadiers, sqAxisLaserGrenadiers, sqAxisNDAKSecurity, sqAxisLaserJagdgrenadiere, sqAxisUbertotenAssaultSquad ],
    [ null, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisHeavyFlakGrenadiers, sqAxisHeavyFlameGren, sqAxisLaserGrenadiers, sqAxisNDAKSecurity, sqAxisUbertotenAssaultSquad ].concat(anyAxis_2ndSectionUnits),
    [ null, sqAxisGorillas, sqAxisZombies, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisHeavyReconGrenadiers, sqAxisUbertotenSuicideSquad ].concat(anyAxis_3rdSectionUnits),
    [ null, sqAxisGorillas, sqAxisZombies, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisLaserGrenadiers, sqAxisNDAKSecurity ].concat(anyAxis_4thSectionUnits)
];
axisBlutkreuzPlatoon.supports = [
    sqAxisBeobachterTeam, sqAxisSniperGrenadierTeam,
    sqAxisJagdgrenadiere,
    sqAxisUbertotenAssaultSquad, sqAxisUbertotenSuicideSquad,
    sqAxisBlutkreuzPionereSquad,
    vhAxisMPWIIALuther, vhAxisMPWIIBLudwig,
    vhAxisMPWIIIAWotan,
    vhAxisTPWVICPrinzluther, vhAxisTPWVIDSturmprinz,
    vhAxisHO347III, vhAxisHO347IV,
    vhAxisHO357V, vhAxisHO357VII,
    vhStrongpoint, vhBunker, vhAxisFestungLaserKanoneAusf47
].concat(vhAxisAnyLPW, vhAxisAnyHPWVI, anyAxis_SupportSectionUnits);

//Panzer Walker Platoon Definition
//NOTE - not currently adding the default 2nd, 3rd, and 4th units to this yet...
var axisPanzerWalkerPlatoon = new PlatoonDefinition('XPWP', 'Panzer Walker Platoon');
axisPanzerWalkerPlatoon.sections = [
    [ sqAxisKommandotrupp, hPanzerprinz, hPanzerprinzAndJagdluther, hGeneralmajorSigridVonThaler, hDesertFox, hTinaAndHyne ],
    [].concat(vhAxisAnyArmoredTransport, vhAxisAnyLPW, vhAxisAnyMPWII),
    [null].concat(vhAxisAnyArmoredTransport, vhAxisAnyLPW, vhAxisAnyMPWII, vhAxisAnyMPWIII),
    [null].concat(vhAxisAnyLPW, vhAxisAnyMPWII, vhAxisAnyMPWIII, vhAxisAnyHPWVI),
    [null].concat(vhAxisAnyMPWII, vhAxisAnyMPWIII, vhAxisAnyHPWVI)
];
axisPanzerWalkerPlatoon.supports = [
    sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, , sqAxisReconGrenadiers, sqAxisNDAKReconGren, sqAxisNDAKTankHunter,
    sqAxisBeobachterTeam,
    vhAxisHO347III, vhAxisHO347IV,
    vhAxisHO357V, vhAxisHO357VII
].concat(anyAxis_SupportSectionUnits);
axisPanzerWalkerPlatoon.additionalSupports = {
    'Transport': [ null, vhAxisTPWVICPrinzluther ]
};

//Axis Afrika nBV NDAK Platoon Definition
var axisAfrikaZBVPlatoon = new PlatoonDefinition('AZBV', 'Afrika nBV "Special Purpose" Platoon');
axisAfrikaZBVPlatoon.sections = [
    [ sqAxisNDAKCommand].concat(hAnyAxis_Leaders),
    [ sqAxisNDAKTankHunter, sqAxisNDAKBattleGren, sqAxisNDAKReconGren, sqAxisNDAKSecurity ],
    [ null, sqAxisNDAKBattleGren, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisBlutkreuzPionereSquad, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisUbertotenSuicideSquad, sqAxisLaserGrenadiers ].concat(anyAxis_2ndSectionUnits),
    [ null, sqAxisNDAKTankHunter, sqAxisHeavyFlameGren, sqAxisNDAKBattleGren, sqAxisNDAKSecurity, sqAxisBlutkreuzPionereSquad, sqAxisBattleGrenadiers, sqAxisNDAKBattleGren, sqAxisUbertotenAssaultSquad, sqAxisReconGrenadiers ].concat(anyAxis_3rdSectionUnits),
    [ null, sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisHeavyFlameGren, sqAxisLaserGrenadiers, sqAxisReconGrenadiers, sqAxisNDAKMortar, sqAxisNDAKMortarAndObservers ].concat(anyAxis_4thSectionUnits)
];
axisAfrikaZBVPlatoon.supports = [
	sqAxisNDAKReconGren, sqAxisNDAKTankHunter, sqAxisNDAKBattleGren,
	vhAxisKV47ROtto,
	vhAxisHPWIVJagdluther, vhAxisMPWIICLeopold, 
	vhAxisMPWIIFSturmLeopold, vhAxisHPWIVEJagdwotan, vhAxisMPWIIFLothTrop,
	vhAxisTPWVIGStummel
].concat(vhAxisAnyHO347, vhAxisAnyHO357, vhAxisAnyLPW, vhAxisAnyMPWII, vhAxisAnyHPWVI, anyAxis_SupportSectionUnits);
axisAfrikaZBVPlatoon.prevalidate = function(platoon)
{
    //  Can take an additional support choice if using a NDAK command squad 
    if (platoon.sections[0].definition == sqAxisNDAKCommand)
    {
        platoon.additionalSupports = 1;
    } else {
    	platoon.additionalSupports = 0;
    }
    return true;
};

//Special Axis Platoons
//Gorilla Platoon Definition
var axisGorillaPlatoon = new PlatoonDefinition('Mrks', 'Markus - Gorillas');
axisGorillaPlatoon.sections = [
    [ hMarkus ],
    [ sqAxisGorillas, sqAxisBlutkreuzPionereSquad ],
    [ null, sqAxisGorillas, sqAxisBlutkreuzPionereSquad ],
    [ null, sqAxisGorillas ],
    [ null, sqAxisGorillas ]
];
axisGorillaPlatoon.upgradesAllowed = false;

//Zombie Platoon Definition
var axisZombiePlatoon = new PlatoonDefinition('Ttnm', 'Totenmeister - Zombies');
axisZombiePlatoon.sections = [
    [ hTotenmeister ],
    [ sqAxisZombies, sqAxisUbertotenAssaultSquad, sqAxisUbertotenSuicideSquad ],
    [ null, sqAxisZombies, sqAxisUbertotenAssaultSquad, sqAxisUbertotenSuicideSquad ],
    [ null, sqAxisZombies, sqAxisUbertotenAssaultSquad, sqAxisUbertotenSuicideSquad ],
    [ null, sqAxisZombies, sqAxisUbertotenAssaultSquad, sqAxisUbertotenSuicideSquad ]
];
axisZombiePlatoon.upgradesAllowed = false;

//Axis Artillerie Gruppe Platoon Definition
var axisArtillerieGruppePlatoon = new PlatoonDefinition('AAGp', 'Artillerie-Gruppe Platoon');
axisArtillerieGruppePlatoon.sections = [
    [ sqAxisNDAKCommand, hRolf, hDesertFox ],
    [ sqAxisNDAKMortar, sqAxisNDAKMortarAndObservers, sqAxisJagdgrenadiere ],
    [ null, sqAxisNDAKMortar, sqAxisNDAKMortarAndObservers, sqAxisJagdgrenadiere, sqAxisNDAKReconGren ],
    [ null, sqAxisNDAKMortar, sqAxisNDAKMortarAndObservers, sqAxisJagdgrenadiere, sqAxisNDAKReconGren ],
    [ null, sqAxisNDAKMortar, sqAxisNDAKMortarAndObservers, vhAxisLPWIEHeinrichTrop ]
];
axisArtillerieGruppePlatoon.supports = [
	sqAxisNDAKReconGren, sqAxisNDAKObserver 
];
axisArtillerieGruppePlatoon.upgradesAllowed = false;
axisArtillerieGruppePlatoon.prevalidate = function(platoon)
{
    //  Can take an additional support choice if using a NDAK command squad 
    if (platoon.sections[0].definition == sqAxisNDAKCommand)
    {
        platoon.additionalSupports = 1;
    } else {
    	platoon.additionalSupports = 0;
    }
    return true;
};

//Axis Faction Definition
var axisFaction = new FactionDefinition(Faction.Axis, 'Axis');
axisFaction.upgrades = [ null, upgAxisDefenses, upgAxisExtraPanzerSupport, upgAxisImplacable,
    upgAxisImprovedCommand, upgAxisLightningWar, upgAxisNebelwerferBarrage, upgAxisSchwerArmor,
    upgAxisPotatoMashers, upgAxisPanzerPioniere, upgAxisDerSchwarm, upgAxisSecondGenerationSerum
];
axisFaction.unitUpgrades = [
    upgUnitAxisGrenades,
    upgUnitAxisHighVisibilityLasers,
    upgUnitAxisOverchargedPowerCells,
    upgUnitAxisAirEnhancedFliegerfaust
];
axisFaction.platoons = [ axisSturmgrenadierePlatoon, axisSchwerPlatoon, axisBlutkreuzPlatoon, axisGorillaPlatoon, axisZombiePlatoon, axisPanzerWalkerPlatoon, axisAfrikaZBVPlatoon, axisArtillerieGruppePlatoon, mercCleaningPlatoon ];

axisFaction.heroes = [ hSigridVonThaler, hLara, hGrenadierX, hMarkus, hManfred, hAngela, hStefan, hTotenmeister, 
                       hPanzerprinz, hPanzerprinzAndJagdluther, hGeneralmajorSigridVonThaler, hHauptmanKlausVonRichthofen, hDesertFox,
                       hTinaAndHyne, hRolf, hAngela_Spy].concat(hMercAnyHeroes);
