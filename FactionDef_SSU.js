//  SSU

//SSU platoon upgrades
var upgSSUCavalryPlatoon    = new UpgradeDefinition('CavP', 'Cavalry Platoon', 5);
var upgSSUImprovedCommand   = new UpgradeDefinition('SICm', 'Improved Command', 5);
upgSSUImprovedCommand.prevalidate = function (platoon)
{
    return platoon.definition == ssuDefensePlatoon
        || platoon.definition == ssuPoliticalPlatoon;
};
upgSSUImprovedCommand.validate = upgAlliesImprovedCommand.validate;
var upgSSUResourceful       = new UpgradeDefinition('Rsrf', 'Resourceful', 15);
var upgSSUPoliticalImportance = new UpgradeDefinition('PImp', 'Political Importance', 5);
upgSSUPoliticalImportance.numCommissars = 2;
var upgSSUTankodesantniki   = new UpgradeDefinition('Tnko', 'Tankodesantniki', 10);
var upgSSUTrainSupport      = new UpgradeDefinition('TrnS', 'Train Support', 20);
var upgSSUCityFighters      = new UpgradeDefinition('CtyF', 'City Fighters', 10);
upgSSUCityFighters.prevalidate = function (platoon)
{
    return platoon.definition == ssuRedPlatoon;
};
upgSSUCityFighters.validate = upgAlliesImprovedCommand.validate;
upgSSURedBanner             = new UpgradeDefinition('RBnr', 'Red Banner', 15);
upgSSUVertushkaFieldTelephones = new UpgradeDefinition('Tlfn', 'Vertushka Field Telephones', 5);
upgSSUZvyezdniScopes        = new UpgradeDefinition('ZScp', 'Zvyezdni Scopes', 5);
upgSSURideOfTheValkyries    = new UpgradeDefinition('Valk', 'Ride of the Valkyries', 25);

//SSU Unit upgrades
var upgUnitSSUNeedsOfTheMany= new UpgradeDefinition('NotM', 'Needs of the Many', 1);
var upgUnitSSUWalkerDissection = new UpgradeDefinition('WkDs', 'Walker Dissection', 3);
var upgUnitSSUGrenadeCharge = new UpgradeDefinition('GrnC', 'Grenade Charge', 2);
var upgUnitSSUGrizzledVeterans = new UpgradeDefinition('GVet', 'Grizzled Veterans', 2);

//SSU Units
//SSU Basic Infantry Units
var sqSSUCommand            = new UnitDefinition('SCmS', '"The Medvedi" SSU Command Squad', 25);
var sqSSUCommissar          = new UnitDefinition('Cmsr', '"The Drakoni" Commissar Squad', 25);

var sqSSUBattleSquad        = new UnitDefinition('BatS', '"Frontoviki" SSU Battle Squad', 20);
var sqSSUCloseCombat        = new UnitDefinition('CCmS', '"Fakyeli" SSU Close Combat Squad', 21);
var sqSSURifleSquad         = new UnitDefinition('RflS', '"Ohotniki" SSU Rifle Squad', 20);
var sqSSUAuxiliaryAttack    = new UnitDefinition('AuxS', '"Chinese Volunteers" SSU Auxiliary Attack Squad', 15);

var sqSSUSniperTeam         = new UnitDefinition('SSnT', '"Jnetzi" SSU Sniper Team', 14);
var sqSSUObserverTeam       = new UnitDefinition('SObs', '"Nadbludatyel" SSU Observer Team', 8);

var sqSSUHeavyMortarTeam    = new UnitDefinition('SHMT', '"Hailstorm" SSU Heavy Mortar Team', 24);
var sqSSUTeslaGunTeam       = new UnitDefinition('STGT', '"Red Lightning" SSU Tesla Gun Team', 22);

//SSU Red Guard Infantry Units
var sqSSURedGuardsCommand   = new UnitDefinition('RGCm', '"Red Command" Red Guards Command Squad', 25);
var sqSSURedGuardsAssaultSquad  = new UnitDefinition('RdAs', '"Red Storm" Red Guards Assault Squad', 26);
var sqSSURedGuardsAntitankSquad = new UnitDefinition('RdAt', '"Red Thunder" Red Guards Antitank Squad', 20);

//SSU Steel Guard Infantry Units
var sqSSUSteelGuardsCommandSquad = new UnitDefinition('StCo', '"Heavy Red Command" Steel Guards Command Squad', 45);
sqSSUSteelGuardsCommandSquad.upgradesAllowed = false;
var sqSSUSteelGuardsAssaultSquad = new UnitDefinition('StAs', '"Red Tornado" Steel Guards Assault Squad', 35);
sqSSUSteelGuardsAssaultSquad.upgradesAllowed = false;
var sqSSUSteelGuardsSniper  = new UnitDefinition('StSn', '"Silent Death" Steel Guards Sniper Squad', 30);
var upgSSUSteelGuardsSniperScopes = new UpgradeDefinition('SGSSc', 'Zvyezdni Scopes', 10);
sqSSUSteelGuardsSniper.upgrades = [ upgSSUSteelGuardsSniperScopes ];
sqSSUSteelGuardsSniper.upgradesAllowed = false;
var sqSSUSteelGuardsFireSupportSquad = new UnitDefinition('StFSS', '"Steel Wall" Steel Guards Fire Support Squad', 40);
sqSSUSteelGuardsFireSupportSquad.upgradesAllowed = false;
var sqSSUSteelGuardsTeslaSquad = new UnitDefinition('StTS', '"Steel Lightning" Steel Guards Tesla Squad', 38);
sqSSUSteelGuardsTeslaSquad.upgradesAllowed = false;
var sqSSUSteelGuardsAntiTankSquad = new UnitDefinition('StATS', '"Steel Thunderbolt" Steel Guards Anti-Tank Squad', 42);
sqSSUSteelGuardsAntiTankSquad.upgradesAllowed = false;
var sqSSUSteelGuardAny_ButCommand = [sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper, sqSSUSteelGuardsFireSupportSquad, 
                          sqSSUSteelGuardsTeslaSquad, sqSSUSteelGuardsAntiTankSquad ];

//SSU Spetsnaz Infantry Units
var sqSSUSpetsnazCommandSquad = new UnitDefinition('SzCS', '"Task Force Command" Spetsnaz Command Squad', 20);
var sqSSUSpetsnazKillSquad = new UnitDefinition('SzKS', '"Killers" Spetsnaz Kill Squad', 15);
var sqSSUSpetsnazSaboteurSquad = new UnitDefinition('SzSS', '"Saboteurs" Spetsnaz Saboteur Squad', 14);
var sqSSUSpetsnazAntiTankSquad = new UnitDefinition('SzSpS', '"Specialists" Spetsnaz Anti-Tank Squad', 23);
var sqSSUSpetsnazAssaultSquad = new UnitDefinition('SzAS', '"Heavies" Spetsnaz Assault Squad', 23);
var sqSSUSpetsnazObserverSquad = new UnitDefinition('SzObS', '"Observers" Spetsnaz Observer Squad', 8);

//SSU Vehicles
var vhSSUKV47ANadya         = new UnitDefinition('KV47A', 'KV47-A "Nadya"', 30, UnitType.Vehicle);
var vhSSUKV47BNatasha       = new UnitDefinition('KV47B', 'KV47-B "Natasha"', 40, UnitType.Vehicle);
var vhSSUKV47CNatalya       = new UnitDefinition('KV47C', 'KV47-C "Natalya"', 30, UnitType.Vehicle);
var vhSSUAnyKV47 = [ vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUKV47CNatalya ];

var vhSSUKV47DNina          = new UnitDefinition('KV47D', 'KV47-D Aero "Nina"', 40, UnitType.Vehicle);
var vhSSUKV47ENikita        = new UnitDefinition('KV47E', 'KV47-E Aero "Nikita"', 45, UnitType.Vehicle);
var vhSSUKV47FNastasia      = new UnitDefinition('KV47F', 'KV47-F Aero "Nastasia"', 40, UnitType.Vehicle);
var vhSSUAnyKV47Aero = [ vhSSUKV47DNina, vhSSUKV47ENikita, vhSSUKV47FNastasia ];

var vhSSUK47GMikhail        = new UnitDefinition('KV47G', 'KV47-G "Mikhail"', 30, UnitType.Vehicle);
var vhSSUK47HMaksim         = new UnitDefinition('KV47H', 'KV47-H "Maksim"', 35, UnitType.Vehicle);
var vhSSUK47IMarlen         = new UnitDefinition('KV47I', 'KV47-I "Marlen"', 35, UnitType.Vehicle);
var vhSSUK47JMelor          = new UnitDefinition('KV47J', 'KV47-J "Melor"', 35, UnitType.Vehicle);
var vhSSUAnyKV47RAD = [ vhSSUK47GMikhail, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor ];

var vhSSUK47KAleksei        = new UnitDefinition('KV47K', 'KV47-K (TSH) "Aleksei"', 30, UnitType.Vehicle);
var vhSSUK47LAnatoly        = new UnitDefinition('KV47L', 'KV47-L (TSH) "Anatoly"', 36, UnitType.Vehicle);
var vhSSUK47BNatashaTSH     = new UnitDefinition('KV47B_T', 'KV47-B (TSH) "Natasha"', 32, UnitType.Vehicle);
var vhSSUK47GMikhailTSH     = new UnitDefinition('KV47G_T', 'KV47-G (TSH) "Mikhail"', 22, UnitType.Vehicle);
var vhSSUK47DNinaTSH        = new UnitDefinition('KV47D_T', 'KV47-D (TSH) "Nina"', 33, UnitType.Vehicle);
var vhSSUAnyKV47TSH = [ vhSSUK47KAleksei, vhSSUK47LAnatoly, vhSSUK47BNatashaTSH, vhSSUK47GMikhailTSH, vhSSUK47DNinaTSH ];

var vhSSUBR47100RedFury     = new UnitDefinition('BR47100', 'BR 47-100 "Red Fury"', 20, UnitType.Vehicle);
var vhSSUBR47200RedRain     = new UnitDefinition('BR47200', 'BR 47-200 "Red Rain"', 20, UnitType.Vehicle);
var vhSSUAnyBR47 = [ vhSSUBR47100RedFury, vhSSUBR47200RedRain ];

var vhSSUAirborneTransport  = new UnitDefinition('MI45', 'MIL MI-45 Airborne Transport', 40, UnitType.Vehicle);
var vhSSUAirborneWalkerTransport = new UnitDefinition('MI46', 'MIL MI-46 Airborne Walker Transport', 40, UnitType.Vehicle);

var vhSSUMI47AStriker       = new UnitDefinition('MI47A', 'MIL MI-47A "Striker"', 55, UnitType.Vehicle);
var vhSSUMI47BBurner        = new UnitDefinition('MI47B', 'MIL MI-47B "Burner"', 50, UnitType.Vehicle);
var vhSSUMI47CAirblaster    = new UnitDefinition('MI47C', 'MIL MI-47C "Airblaster"', 50, UnitType.Vehicle);
var vhSSUMI47DDeathRain     = new UnitDefinition('MI47D', 'MIL MI-47D "Death Rain"', 45, UnitType.Vehicle);
var vhSSUAnyMI47 = [ vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain ];

var vhSSUIS5AMaoZedong      = new UnitDefinition('IS5A', 'IS-5 A "Mao Zedong"', 85, UnitType.Vehicle);
var vhSSUIS5BVladimirLenin  = new UnitDefinition('IS5B', 'IS-5 B "Vladimir Lenin"', 90, UnitType.Vehicle);
var vhSSUIS5CIosefStalin   = new UnitDefinition('IS5C', 'IS-5 C "Iosef Stalin"', 78, UnitType.Vehicle);
var vhSSUIS5DAleksandrVasilevsky   = new UnitDefinition('IS5D', 'IS-5 D "Aleksandr Vasilevsky"', 100, UnitType.Vehicle);
var vhSSUAnyIS5 = [ vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin, vhSSUIS5CIosefStalin, vhSSUIS5DAleksandrVasilevsky ];

var vhSSUIS48AKarlMarx      = new UnitDefinition('IS48A', 'IS-48 A "Karl Marx"', 91, UnitType.Vehicle);
var vhSSUIS48BLavrentiyBeria= new UnitDefinition('IS48B', 'IS-48 B "Lavrentiy Beria"', 85, UnitType.Vehicle);
var vhSSUAnyIS48 = [ vhSSUIS48AKarlMarx, vhSSUIS48BLavrentiyBeria ];

var vhSSUISU203SergeiShtemenko = new UnitDefinition('ISU203', 'ISU-203 "Sergei Shtemenko"', 100, UnitType.Vehicle);
var vhSSUAnyISU203 = [ vhSSUISU203SergeiShtemenko ];

var vhSSUKV3KMatrioshka     = new UnitDefinition('KV3K', 'KV-3 K "Matrioshka"', 65, UnitType.Vehicle);
var vhSSUKV3MBabushka       = new UnitDefinition('KV3M', 'KV-3 M "Babushka"', 50, UnitType.Vehicle);
var vhSSUAnyKV3 = [ vhSSUKV3KMatrioshka, vhSSUKV3MBabushka ];

var vhSSUMotherlandFortificationTeslaModel = new UnitDefinition('MFTM', 'Motherland Fortification Tesla Model', 25, UnitType.Vehicle);
var upgVhMFTMDualTeslaGun   = new UpgradeDefinition('MFDT', 'Dual Tesla Gun', 10);
var upgVhMFTMBunker         = new UpgradeDefinition('MFTB', 'Bunker', 15);
vhSSUMotherlandFortificationTeslaModel.upgrades = [
    upgVhMFTMDualTeslaGun, upgVhMFTMBunker
];

//Capturable vehicles definition
var vhSSUCapturable = [ vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUKV47CNatalya, vhSSUKV47DNina, vhSSUKV47ENikita, vhSSUKV47FNastasia,vhSSUK47GMikhail, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor, vhSSUK47KAleksei, vhSSUK47BNatashaTSH, vhSSUK47GMikhailTSH, vhSSUK47DNinaTSH, vhSSUKV3KMatrioshka, vhSSUKV3MBabushka, vhSSUBR47100RedFury, vhSSUBR47200RedRain, vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin, vhSSUIS5CIosefStalin, vhSSUIS48BLavrentiyBeria, vhSSUISU203SergeiShtemenko ];

//SSU Heroes
//  vhGrandma               = new UnitDefinition('Grma', 'Grand`'ma', 35, UnitType.Vehicle);
var hKoshkaRudinova         = new UnitDefinition('KshR', 'Koshka Rudinova', 18, UnitType.Hero);
//  Grand`ma isn't a unit upgrade... what if Heros can eventually have upgrades?  This would conflict.
//  hKoshkaRudinova.upgrades = [ new UpgradeDefinition('Grma', 'Grand\'ma', 35) ];
var hKoshkaRudinovaGrandma  = new UnitDefinition('KshRG', 'Koshka Rudinova with Grand\'ma', 18 + 35, UnitType.Hero);
var hNikolai                = new UnitDefinition('Nkli', 'Nikolaï', 20, UnitType.Hero);
var hYakov                  = new UnitDefinition('Ykov', 'Yakov', 21, UnitType.Hero);
var hRedYana                = new UnitDefinition('RYna', 'Red Yana', 22, UnitType.Hero);
var hWinterChild            = new UnitDefinition('WChd', '"Winter Child" Colonel Ivan Vasiliev', 70, UnitType.Hero);
var hTheRedBanner			= new UnitDefinition("RdBnr", 'The Red Banner', 16, UnitType.Hero);
var hIvanTheButcher			= new UnitDefinition("IvanB", 'Ivan the Butcher', 20, UnitType.Hero);
var hIronJoe				= new UnitDefinition("IronJ", 'Iron Joe', 20, UnitType.Hero);
var hRedYana_Starshina		= new UnitDefinition("RdYnS", 'Red Yana (Starshina)', 22, UnitType.Hero);

var hLeaderAbility			= [ hKoshkaRudinova, hKoshkaRudinovaGrandma, hNikolai, hYakov, hTheRedBanner, hIronJoe, hRedYana_Starshina ];

hKoshkaRudinova.mutualExclusive = [ hKoshkaRudinovaGrandma ];
hKoshkaRudinovaGrandma.mutualExclusive = [ hKoshkaRudinova ];

//SSU Solo Commissars
var soloSSUCommissarFonvizin    = new UnitDefinition('CFon', '"The Fonvizin"', 9, UnitType.Solo);
var soloSSUCommissarSumarokov   = new UnitDefinition('CSum', '"The Sumarokov"', 5, UnitType.Solo);
var soloSSUCommissarDerzhavin   = new UnitDefinition('CDer', '"The Derzhavin"', 5, UnitType.Solo);
var soloSSUCommissarTrediakovsky= new UnitDefinition('CTrd', '"The Trediakovsky"', 8, UnitType.Solo);
var soloSSUCommissarKaramzin    = new UnitDefinition('CKrm', '"The Karamzin"', 8, UnitType.Solo);

var ssuCommissars = [
    soloSSUCommissarFonvizin,
    soloSSUCommissarSumarokov,
    soloSSUCommissarDerzhavin,
    soloSSUCommissarTrediakovsky,
    soloSSUCommissarKaramzin
];

//
sqSSUCommissar.supports = [ null, vhSSUAirborneTransport ];
sqSSUSteelGuardsCommandSquad.supports = [ null ].concat(vhSSUAnyKV47, vhSSUAnyKV47Aero, vhSSUAnyKV47RAD);

sqSSUCommand.numCommissars = 2;
sqSSURedGuardsCommand.numCommissars = 2;

//Default Section 
var anySSU2ndSectionUnits_ExceptSteelGuard = [ sqSSUSpetsnazKillSquad ].concat(anyMerc_2ndSectionUnits); 
var anySSU3rdSectionUnits_ExceptSteelGuard = [ sqSSUSpetsnazSaboteurSquad ].concat(anyMerc_3rdSectionUnits); 
var anySSU4thSectionUnits_ExceptSteelGuard = [ sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad ].concat(anyMerc_4thSectionUnits); 
var anySSUSteelGuardSupportUnits = [ sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad ].concat(anyMerc_SupportSectionUnits);
var anySSUSupportSectionUnits = [ sqSSUSpetsnazAntiTankSquad, sqSSUSpetsnazAssaultSquad, sqSSUSteelGuardsFireSupportSquad, sqSSUSteelGuardsTeslaSquad, 
                               sqSSUSteelGuardsAntiTankSquad, vhSSUK47KAleksei, vhSSUK47LAnatoly ].concat(anyMerc_SupportSectionUnits);

//SSU Defense Platoon Definition
var ssuDefensePlatoon = new PlatoonDefinition('SDef', 'Defense Platoon');
ssuDefensePlatoon.sections = [
    [ sqSSUCommand, hKoshkaRudinova, hKoshkaRudinovaGrandma, hYakov, hIronJoe, hRedYana_Starshina],
    [ sqSSUBattleSquad, sqSSUCloseCombat, sqSSURifleSquad ],
    [ null, sqSSUBattleSquad, sqSSUCloseCombat, sqSSUAuxiliaryAttack ].concat(anySSU2ndSectionUnits_ExceptSteelGuard),
    [ null, sqSSUBattleSquad, sqSSURifleSquad, sqSSUAuxiliaryAttack, sqSSUSpetsnazAssaultSquad ].concat(anySSU3rdSectionUnits_ExceptSteelGuard),
    [ null, sqSSUCloseCombat, sqSSURifleSquad, sqSSUAuxiliaryAttack, sqSSUSpetsnazAssaultSquad ].concat(anySSU4thSectionUnits_ExceptSteelGuard)
];
ssuDefensePlatoon.supports = [
    sqSSUObserverTeam, sqSSUSniperTeam,
    sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam,
    sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper,
    vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUK47BNatashaTSH, vhSSUKV47CNatalya,
    vhSSUKV47DNina, vhSSUK47DNinaTSH, vhSSUKV47ENikita, vhSSUKV47FNastasia,
    vhSSUK47GMikhail, vhSSUK47GMikhailTSH, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor,
    vhSSUAirborneTransport, vhSSUAirborneWalkerTransport,
    vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain,
    vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin, vhSSUIS5CIosefStalin, vhSSUIS5DAleksandrVasilevsky, vhSSUISU203SergeiShtemenko,
    vhSSUIS48AKarlMarx, vhSSUIS48BLavrentiyBeria,
    vhSSUKV3KMatrioshka, vhSSUKV3MBabushka,
    vhSSUBR47100RedFury, vhSSUBR47200RedRain,
    vhStrongpoint, vhBunker, vhSSUMotherlandFortificationTeslaModel
].concat(anySSUSupportSectionUnits);

//SSU Political Platoon Definition
var ssuPoliticalPlatoon = new PlatoonDefinition('SPol', 'Political Platoon');
ssuPoliticalPlatoon.sections = [
    [ sqSSUCommissar, hNikolai, hRedYana_Starshina ],
    [ sqSSUAuxiliaryAttack, sqSSUBattleSquad, sqSSUCloseCombat, sqSSURifleSquad ],
    [ null, sqSSUAuxiliaryAttack, sqSSUCloseCombat, sqSSURifleSquad ].concat(anySSU2ndSectionUnits_ExceptSteelGuard),
    [ null, sqSSUAuxiliaryAttack, sqSSUBattleSquad, sqSSURifleSquad, sqSSUSpetsnazAntiTankSquad ].concat(anySSU3rdSectionUnits_ExceptSteelGuard),
    [ null, sqSSUAuxiliaryAttack, sqSSUBattleSquad, sqSSUCloseCombat, sqSSUSpetsnazAntiTankSquad ].concat(anySSU4thSectionUnits_ExceptSteelGuard)
];
ssuPoliticalPlatoon.supports = [
    sqSSUObserverTeam, sqSSUSniperTeam,
    sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam,
    sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper,
    vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUK47BNatashaTSH, vhSSUKV47CNatalya,
    vhSSUK47GMikhail, vhSSUK47GMikhailTSH, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor,
    vhSSUAirborneTransport,
    vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain,
    vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin, vhSSUIS5CIosefStalin, vhSSUIS5DAleksandrVasilevsky, vhSSUISU203SergeiShtemenko,
    vhSSUKV3KMatrioshka, vhSSUKV3MBabushka,
    vhSSUBR47100RedFury, vhSSUBR47200RedRain,
    vhStrongpoint, vhBunker, vhSSUMotherlandFortificationTeslaModel
].concat(anySSUSupportSectionUnits);

//SSU Red Platoon Definition
var ssuRedPlatoon = new PlatoonDefinition('SRed', 'Red Platoon');
ssuRedPlatoon.sections = [
    [ sqSSURedGuardsCommand, hKoshkaRudinova, hKoshkaRudinovaGrandma, hRedYana_Starshina ],
    [ sqSSURedGuardsAntitankSquad, sqSSURedGuardsAssaultSquad ],
    [ null, sqSSURedGuardsAntitankSquad, sqSSURedGuardsAssaultSquad ].concat(anySSU2ndSectionUnits_ExceptSteelGuard),
    [ null, sqSSUSpetsnazAntiTankSquad, sqSSUSpetsnazAssaultSquad, sqSSUSteelGuardsAssaultSquad,
        vhSSUKV3KMatrioshka, vhSSUKV3MBabushka,
        vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUK47BNatashaTSH, vhSSUKV47CNatalya,
        vhSSUKV47DNina, vhSSUK47DNinaTSH, vhSSUKV47ENikita, vhSSUKV47FNastasia,
        vhSSUK47GMikhail, vhSSUK47GMikhailTSH, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor,
        vhSSUIS48AKarlMarx, vhSSUIS48BLavrentiyBeria, vhSSUIS5CIosefStalin, vhSSUIS5DAleksandrVasilevsky, vhSSUISU203SergeiShtemenko ].concat(anySSU3rdSectionUnits_ExceptSteelGuard),
    [ null, sqSSUSpetsnazAntiTankSquad, sqSSUSpetsnazAssaultSquad, sqSSUSteelGuardsAssaultSquad,
        vhSSUKV47ANadya, vhSSUKV47BNatasha, vhSSUK47BNatashaTSH, vhSSUKV47CNatalya,
        vhSSUK47GMikhail, vhSSUK47GMikhailTSH, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor,
        vhSSUIS5AMaoZedong, vhSSUIS5BVladimirLenin,
        vhSSUMI47AStriker, vhSSUMI47BBurner, vhSSUMI47CAirblaster, vhSSUMI47DDeathRain,
        vhSSUAirborneWalkerTransport ].concat(anySSU4thSectionUnits_ExceptSteelGuard)
];
ssuRedPlatoon.supports = [
    sqSSUObserverTeam, sqSSUSniperTeam,
    sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam,
    sqSSUBattleSquad, sqSSUCloseCombat, sqSSURifleSquad,
    sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper,
    vhSSUBR47100RedFury, vhSSUBR47200RedRain,
    vhSSUK47GMikhail, vhSSUK47GMikhailTSH, vhSSUK47HMaksim, vhSSUK47IMarlen, vhSSUK47JMelor,
    vhStrongpoint, vhBunker, vhSSUMotherlandFortificationTeslaModel
].concat(anySSUSupportSectionUnits);

ssuRedPlatoon.validate = function(platoon)
{
    //  Cannot take 4th Section without a 2nd Section
    if (platoon.sections[4] != null &&
        platoon.sections[2] == null)
    {
        ForceError("SSU Red Platoon requires 2nd Section to be taken before a 4th Section may be taken.");
        return false;
    }
    return true;
};

//SSU Steel Guard Platoon Definition
var ssuSteelGuardsPlatoon = new PlatoonDefinition('SStl', 'Steel Guards Platoon');
ssuSteelGuardsPlatoon.sections = [
    [ sqSSUSteelGuardsCommandSquad, hRedYana_Starshina ],
    [ sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsFireSupportSquad ],
    [ null, sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper, sqSSUSteelGuardsFireSupportSquad ],
    [ null, sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsTeslaSquad, sqSSUSteelGuardsAntiTankSquad, sqSSURedGuardsAssaultSquad, sqSSURedGuardsAntitankSquad ].concat(vhSSUAnyKV47, vhSSUAnyKV47Aero, vhSSUAnyKV47RAD),
    [ null, sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper, sqSSUSteelGuardsFireSupportSquad, sqSSUSteelGuardsTeslaSquad, sqSSUSteelGuardsAntiTankSquad, sqSSURedGuardsAssaultSquad, sqSSURedGuardsAntitankSquad ].concat(vhSSUAnyKV47, vhSSUAnyKV47Aero, vhSSUAnyKV47RAD, vhSSUAnyKV3, vhSSUAnyIS48)
];
ssuSteelGuardsPlatoon.supports = [
    sqSSUBattleSquad, sqSSURifleSquad, sqSSUCloseCombat, sqSSUAuxiliaryAttack,
    sqSSUObserverTeam, sqSSUTeslaGunTeam, sqSSUHeavyMortarTeam,
    sqSSUSteelGuardsAssaultSquad, sqSSUSteelGuardsSniper
].concat(vhSSUAnyKV47, vhSSUAnyKV47Aero, vhSSUAnyKV47RAD,
    vhSSUAnyBR47, vhSSUAnyKV3, vhSSUAnyIS5, vhSSUAnyIS48,
    vhSSUAirborneTransport, vhSSUAirborneWalkerTransport,
    vhSSUAnyMI47, anySSUSteelGuardSupportUnits, anySSUSupportSectionUnits);

//Spetsnaz Platoon Definition
var ssuSpetsnazPlatoon = new PlatoonDefinition('SSpnz', 'Spetsnaz Special Operations Platoon');
ssuSpetsnazPlatoon.sections = [
    [ sqSSUSpetsnazCommandSquad ].concat(hLeaderAbility),
    [ sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad, sqSSUSpetsnazAntiTankSquad ],
    [ null, sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad, sqSSUSpetsnazAssaultSquad ].concat(anySSU2ndSectionUnits_ExceptSteelGuard),
    [ null, sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad, sqSSUSpetsnazAssaultSquad, sqSSURedGuardsAssaultSquad, sqSSURedGuardsAntitankSquad ].concat(vhSSUAnyKV47Aero, vhSSUAnyKV47TSH, vhSSUAnyKV47RAD, anySSU3rdSectionUnits_ExceptSteelGuard),
    [ null, sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad, sqSSUSpetsnazAntiTankSquad, sqSSURedGuardsAssaultSquad, sqSSURedGuardsAntitankSquad ].concat(vhSSUAnyKV47Aero, vhSSUAnyKV47TSH, vhSSUAnyKV47RAD, anySSU4thSectionUnits_ExceptSteelGuard),
];
ssuSpetsnazPlatoon.supports = [
    sqSSUSpetsnazKillSquad, sqSSUSpetsnazSaboteurSquad, sqSSUSpetsnazObserverSquad, sqSSUSpetsnazAntiTankSquad, sqSSUSpetsnazAssaultSquad
].concat(sqSSUSteelGuardAny_ButCommand, vhSSUAnyKV47, vhSSUAnyKV47Aero, vhSSUAnyKV47TSH, vhSSUAnyKV47RAD,
    vhSSUAnyKV3, vhSSUAnyISU203, vhSSUAnyIS5, vhSSUAnyIS48,
    vhSSUAirborneTransport, vhSSUAirborneWalkerTransport,
    vhSSUAnyMI47 ); //NOTE - anySSUSupportSectionUnits removed due to overlap with others in this platoon
//Handle the platoon defined upgrades
ssuSpetsnazPlatoon.limitedUpgrades = [ null, upgSSUCavalryPlatoon, upgSSUImprovedCommand, upgSSUResourceful, upgSSUCityFighters ];
//Handle the additional support choice
ssuSpetsnazPlatoon.prevalidate = function(platoon)
{
    //  Can take an additional support choice if using a spetsnaz command squad 
    if (platoon.sections[0].definition == sqSSUSpetsnazCommandSquad)
    {
        platoon.additionalSupports = 1;
    } else {
    	platoon.additionalSupports = 0;
    }
    return true;
};

//Udarny Weapons Platoon Definition - Special Platoon
var ssuUdarnyWeaponsPlatoon = new PlatoonDefinition('UdWpns', 'Udarny Weapons Platoon');
ssuUdarnyWeaponsPlatoon.sections = [
    [ sqSSUCommand, hIronJoe ],
    [ sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam ],
    [ null, sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam ].concat(vhSSUAnyBR47),
    [ null, sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam, sqSSUSpetsnazAssaultSquad ].concat(vhSSUAnyBR47),
    [ null, sqSSUHeavyMortarTeam, sqSSUTeslaGunTeam ].concat(vhSSUAnyBR47),
];
ssuUdarnyWeaponsPlatoon.supports = [
	sqSSUSpetsnazAssaultSquad, sqSSUSpetsnazObserverSquad,
];
//Handle platoon upgrades and commissars
ssuUdarnyWeaponsPlatoon.commissarsAllowed = false;
ssuUdarnyWeaponsPlatoon.upgradesAllowed = false;
//Handle the additional support choice
ssuUdarnyWeaponsPlatoon.prevalidate = function(platoon)
{
    //  Can take an additional support choice if using a medvedi command squad 
    if (platoon.sections[0].definition == sqSSUCommand)
    {
        platoon.additionalSupports = 1;
    } else {
    	platoon.additionalSupports = 0;
    }
    return true;
};

//SSU Faction Definition
var ssuFaction = new FactionDefinition(Faction.SSU, 'SSU');
ssuFaction.upgrades = [ null, upgSSUCavalryPlatoon, upgSSUImprovedCommand, upgSSUResourceful,
    upgSSUPoliticalImportance, upgSSUTankodesantniki, upgSSUTrainSupport,
    upgSSUCityFighters, upgSSURedBanner, upgSSUVertushkaFieldTelephones,
    upgSSUZvyezdniScopes, upgSSURideOfTheValkyries
];
ssuFaction.unitUpgrades = [ upgUnitSSUNeedsOfTheMany, upgUnitSSUWalkerDissection, upgUnitSSUGrenadeCharge, upgUnitSSUGrizzledVeterans ];
ssuFaction.platoons = [ ssuDefensePlatoon, ssuPoliticalPlatoon, ssuRedPlatoon, ssuSteelGuardsPlatoon, ssuSpetsnazPlatoon, ssuUdarnyWeaponsPlatoon, mercCleaningPlatoon ];
ssuFaction.heroes = [ hKoshkaRudinova, hKoshkaRudinovaGrandma, hNikolai, hYakov, hRedYana, hWinterChild, hTheRedBanner, hIvanTheButcher, hIronJoe, hRedYana_Starshina ].concat(hMercAnyHeroes);
