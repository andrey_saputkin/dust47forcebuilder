//  ALLIES
//  - Commands
var sqAlliesRangerCommand   = new UnitDefinition('RCom', '"The Boss" Ranger Command Squad', 25);
var sqAlliesHeavyRangerCommand  = new UnitDefinition('HRCm', '"Corps Officers" Heavy Ranger Command Squad', 30);
var sqAlliesRangerAttack    = new UnitDefinition('RAtk', '"Hell Boys" Ranger Attack Squad', 25);
var sqAlliesUSMCCommand	    = new UnitDefinition('MCCm', '"Mustangs" USMC Command Squad', 16);

var sqAlliesAssaultRanger   = new UnitDefinition('AssR', '"BBQ Squad" Assault Ranger Squad', 21);
var sqAlliesCombatRanger    = new UnitDefinition('CmbR', '"The Gunners" Combat Ranger Squad', 17);
sqAlliesCombatRanger.upgrades = [ new UpgradeDefinition('CmbR+', 'Replace one M1+UGL with M9 Bazooka', 3) ];
var sqAlliesRangerWeapon    = new UnitDefinition('RnWp', '"Death Dealers" Ranger Weapon Squad', 20);
var sqAlliesReconRanger     = new UnitDefinition('RcnR', '"Recon Boys" Recon Ranger Squad', 16);

var sqAlliesUSMCRifle 		= new UnitDefinition('MCRS', '"Mavericks" USMC Rifle Squad', 17);
var sqAlliesUSMCDemo	    = new UnitDefinition('MCDT', '"The Saints" USMC Demolition Squad', 18);
var sqAlliesUSMCFireSquad   = new UnitDefinition('MCFS', '"Devil Dogs" USMC Fire Squad', 15);
var sqAlliesUSMCForceRecon  = new UnitDefinition('MCFR', '"Force Recon" USMC Recon Squad', 16);
var sqAlliesUSMCAssault     = new UnitDefinition('MCAs', '"Hell Blazers" USMC Assault Squad', 23);
var sqAlliesUSMCAntiTank    = new UnitDefinition('MCAT', '"Hotshots" USMC Anit-Tank Squad', 20);
var sqAlliesUSMCObserver 	= new UnitDefinition('MCOb', '"Artillery Scouts" USMC Observer Squad', 10);
var sqAlliesSteelMarines    = new UnitDefinition('MCSM', '"Steel Marines" USMC Heavy Engineer Squad', 32);

var sqAlliesRangerSniperTeam= new UnitDefinition('RSnp', '"Crack Shots" Sniper Team', 12);
var sqAlliesRangerObserverTeam  = new UnitDefinition('RObs', '"13 Foxtrot" Observer Team', 8);

var sqAlliesHeavyRangerAssault  = new UnitDefinition('HRAs', '"The Hammers" Heavy Ranger Assault Squad', 28);
var sqAlliesHeavyRangerAttack   = new UnitDefinition('HRAt', '"Grim Reapers" Heavy Ranger Attack Squad', 30);
var sqAlliesHeavyRangerTankHunter = new UnitDefinition('HRTH', '"Tank Busters" Heavy Ranger Tank Hunter Squad', 35);
var sqAlliesBritishParatroops   = new UnitDefinition('BPra', '"Red Devils" British Paratroops', 32);

var sqAlliesBritishCommandosKillSquad   = new UnitDefinition('BCKS', '"Devil\'s Own" British Commandos Kill Squad', 23);
var sqAlliesFrenchForeignLegionKillSquad= new UnitDefinition('FFLK', '"Legio Patria Nostra" French Foreign Legion Kill Squad', 26);

var sqAlliesUSMCHeavyMachineGunTeam = new UnitDefinition('HMGT', '"The Choppers" USMC Heavy Machine Gun Team', 13);
var sqAlliesUSMCHeavyMortarTeam     = new UnitDefinition('HMrT', '"Leathernecks" USMC Heavy Mortar Team', 17);

//Allied vehicles
var vhAlliesM1AWildfire     = new UnitDefinition('M1A',  'LAW M1-A "Wildfire"', 20, UnitType.Vehicle);
var vhAlliesM1BBlackhawk    = new UnitDefinition('M1B',  'LAW M1-B "Blackhawk"', 25, UnitType.Vehicle);
var vhAlliesM1CHoney        = new UnitDefinition('M1C',  'LAW M1-C "Honey"', 25, UnitType.Vehicle);
var vhAlliesM1DBushmaster   = new UnitDefinition('M1D',  'LAW M1-D "Bushmaster"', 30, UnitType.Vehicle);
var vhAlliesAnyM1LAW = [ vhAlliesM1AWildfire, vhAlliesM1BBlackhawk, vhAlliesM1CHoney, vhAlliesM1DBushmaster ];

var vhAlliesM2AMickey       = new UnitDefinition('M2A',  'MCW M2-A "Mickey"', 35, UnitType.Vehicle);
var vhAlliesM2BHotDog       = new UnitDefinition('M2B',  'MCW M2-B "Hot Dog"', 30, UnitType.Vehicle);
var vhAlliesM2CPounder      = new UnitDefinition('M2C',  'MCW M2-C "Pounder"', 40, UnitType.Vehicle);
var vhAlliesM2FSteelRain    = new UnitDefinition('M2F',  'MCW M2-F "Steel Rain"', 45, UnitType.Vehicle);
var vhAlliesM2GBarkingDogLight   = new UnitDefinition('M2GL',  'MCW M2-G "Barking Dog (Light)"', 40, UnitType.Vehicle);
var vhAlliesM2A2MickeyLight   = new UnitDefinition('M2A2',  'MCW M2-A2 "Mickey (Light)"', 30, UnitType.Vehicle);
var vhAlliesM2F2SteelRainLight   = new UnitDefinition('M2F2',  'MCW M2-F2 "Steel Rain (Light)"', 34, UnitType.Vehicle);
var vhAlliesM2C2PounderLight   = new UnitDefinition('M2C2',  'MCW M2-C2 "Pounder (Light)"', 35, UnitType.Vehicle);
var vhAlliesAnyM2MCW = [ vhAlliesM2AMickey, vhAlliesM2BHotDog, vhAlliesM2CPounder, vhAlliesM2FSteelRain, vhAlliesM2GBarkingDogLight, vhAlliesM2A2MickeyLight, vhAlliesM2F2SteelRainLight, vhAlliesM2C2PounderLight ];

var vhAlliesM3DCobra        = new UnitDefinition('M3D',  'MCW M3-D "Cobra"', 35, UnitType.Vehicle);
var vhAlliesM3ERattler      = new UnitDefinition('M3E',  'MCW M3-E "Rattler"', 30, UnitType.Vehicle);
var upgVhM3Amphibious = new UpgradeDefinition('M3Am', 'M3 Amphibious Upgrade', 3);
vhAlliesM3DCobra.upgrades = [ upgVhM3Amphibious ];
vhAlliesM3ERattler.upgrades = [ upgVhM3Amphibious ];
var vhAlliesAnyM3MCW = [ vhAlliesM3DCobra, vhAlliesM3ERattler ];

var vhAlliesM5ASixShooter   = new UnitDefinition('M5A',  'HDW M5-A "The Six Shooter"', 60, UnitType.Vehicle);
var vhAlliesM5BBulldog      = new UnitDefinition('M5B',  'HDW M5-B "Bulldog"', 60, UnitType.Vehicle);
var vhAlliesAnyM5HDW = [ vhAlliesM5ASixShooter, vhAlliesM5BBulldog ];

var vhAlliesP48CDivingDotty = new UnitDefinition('P48C', 'P-48C "Diving Dotty"', 45, UnitType.Vehicle);
var vhAlliesP48XBellowingBertie = new UnitDefinition('P48X', 'P-48X "Bellowing Bertie"', 45, UnitType.Vehicle);
var vhAlliesAnyP48Aircraft  = [ vhAlliesP48CDivingDotty, vhAlliesP48XBellowingBertie ];

var vhAlliesM7LongTomII     = new UnitDefinition('M7', 'HSW M7 "Long Tom II"', 65, UnitType.Vehicle);
var vhAlliesM7Devestator    = new UnitDefinition('M7D', 'HSW M7D "Devestator"', 95, UnitType.Vehicle);
var vhAlliesAnyM7HAW 		= [vhAlliesM7LongTomII, vhAlliesM7Devestator];

var vhAlliesM6APunisher     = new UnitDefinition('M6A',  'HAW M6-A "Punisher"', 100, UnitType.Vehicle);
var vhAlliesM6BFireball     = new UnitDefinition('M6B',  'HAW M6-B "Fireball"', 85, UnitType.Vehicle);
var vhAlliesAnyM6HAW = [ vhAlliesM6APunisher, vhAlliesM6BFireball, vhAlliesM7Devestator];

var vhAlliesM1MobileHQ      = new UnitDefinition('M1', 'HCW M1 "Mobile HQ"', 40, UnitType.Vehicle);

var vhAlliesM9Skysweeper    = new UnitDefinition('M9', 'HSW M9 "Skysweeper"', 65, UnitType.Vehicle);

var vhAlliesM47FieldPhaserStrongpoint = new UnitDefinition('M47S', 'M47 Field Phaser Strongpoint', 28, UnitType.Vehicle);
var upgVhM47DualPhaserGun   = new UpgradeDefinition('M47DP', 'Dual 180W Phaser Gun', 10);
var upgVhM47Bunker          = new UpgradeDefinition('M47Bk', 'Bunker', 15);
vhAlliesM47FieldPhaserStrongpoint.upgrades = [
    upgVhM47DualPhaserGun, upgVhM47Bunker
];

//Capturable vehicles definition
var vhAlliesCapturable = [ vhAlliesM1AWildfire, vhAlliesM1BBlackhawk, vhAlliesM1DBushmaster, vhAlliesM2AMickey, vhAlliesM2BHotDog, vhAlliesM2CPounder, vhAlliesM2FSteelRain, vhAlliesM2GBarkingDogLight, vhAlliesM2A2MickeyLight, vhAlliesM2F2SteelRainLight, vhAlliesM2C2PounderLight, vhAlliesM3ERattler, vhAlliesM5ASixShooter, vhAlliesM7LongTomII, vhAlliesM6APunisher, vhAlliesM6BFireball, vhAlliesM1MobileHQ, vhAlliesM9Skysweeper ];

//Heroes
var hBazookaJoe             = new UnitDefinition('BJoe', 'Bazooka Joe', 15, UnitType.Hero);
var hRhino                  = new UnitDefinition('Rhno', 'Rhino', 22, UnitType.Hero);
var hActionJackson          = new UnitDefinition('AcJk', 'Action Jackson', 24, UnitType.Hero);
var hRosie                  = new UnitDefinition('Rosi', 'Rosie', 21, UnitType.Hero);
var hOzz117                 = new UnitDefinition('O117', 'Ozz 117', 23, UnitType.Hero);
var hThePriest              = new UnitDefinition('Prst', 'The Priest', 19, UnitType.Hero);
var hJohnnyOneEye           = new UnitDefinition('JnyI', 'Johnny One-Eye', 17, UnitType.Hero);
var hTheChef                = new UnitDefinition('Chef', 'The Chef', 20, UnitType.Hero);
var hSergeantVictory        = new UnitDefinition('SgtV', 'Sergeant Victory', 45, UnitType.Hero);
var hLieutenantColonelJosephBrown = new UnitDefinition('LClJB', 'Lieutenant Colonel Joseph Brown', 22, UnitType.Hero);

var hMacheteMack	        = new UnitDefinition('McMk', 'Machete Mack', 22, UnitType.Hero);
var hQuietJack		        = new UnitDefinition('QtJk', 'Quiet Jack', 18, UnitType.Hero);
var hLittleMack 			= new UnitDefinition('LtMk', 'Little Mack', 22, UnitType.Hero);
var hCrazyJimmy 			= new UnitDefinition('CrJm', 'Crazy Jimmy', 20, UnitType.Hero);

hLieutenantColonelJosephBrown.mutualExclusive = [ hBazookaJoe ];
hBazookaJoe.mutualExclusive = [ hLieutenantColonelJosephBrown ];

//Leader definitions
var hAlliesLeaders = [ hBazookaJoe, hRhino, hActionJackson, hMacheteMack, hOzz117, hJohnnyOneEye, hLieutenantColonelJosephBrown, hLittleMack, hCrazyJimmy];
var hAlliesCanLeadAnyPlatoon = [hMacheteMack, hLittleMack, hQuietJack];

//Default Section 
var anyAllied_2ndSectionUnits = [ sqAlliesUSMCRifle, sqAlliesUSMCAssault ].concat(anyMerc_2ndSectionUnits); 
var anyAllied_3rdSectionUnits = [ sqAlliesUSMCRifle ].concat(anyMerc_3rdSectionUnits); 
var anyAllied_4thSectionUnits = [  ].concat(anyMerc_4thSectionUnits); 
var anyAllied_SupportSectionUnits = [ sqAlliesUSMCFireSquad, vhAlliesM1DBushmaster ].concat(anyMerc_SupportSectionUnits);


//Allied Upgrades
var upgAlliesAddtlResources = new UpgradeDefinition('AdRs', 'Additional Resources', 10);
var upgAlliesAirDrop        = new UpgradeDefinition('ArDp', 'Air Drop', 5);
var upgAlliesImprovedCommand= new UpgradeDefinition('AICm', 'Improved Command', 5);
upgAlliesImprovedCommand.prevalidate = function (platoon)
{
    return platoon.definition == alliesCombatPlatoon
        || platoon.definition == alliesAssaultPlatoon;
};
upgAlliesImprovedCommand.validate = function (platoon)
{
    if (platoon.sections[0].definition != platoon.definition.sections[0][0])
    {
        ForceError("The '" + this.name + "' Platoon Upgrade requires " + platoon.definition.sections[0][0].name + " as your Command Section.");
        return false;
    }
    return true;
};
var upgAlliesAeroncaStrike  = new UpgradeDefinition('GB9S', 'Aeronca GB-9 Strike', 30);
var upgAlliesExtraHero      = new UpgradeDefinition('XHro', 'Extra Hero', 5);
upgAlliesExtraHero.bonusHero = true;
upgAlliesExtraHero.validate = function (platoon)
{
    var numExtraHero = 0;
    for (var p in Force.platoons)
    {
        if (Force.platoons[p].upgrade == upgAlliesExtraHero)
        {
            if (++numExtraHero > 1)
            {
                ForceError("The 'Extra Hero' Platoon Upgrade may only be taken once.");
                return false;
            }
            if (Force.platoons[p] == platoon)
                break;
        }
    }

    return true;
};
var upgAlliesPreparatoryBarrage = new UpgradeDefinition('PrpB', 'Preparatory Barrage', 20);
var upgAlliesMedicMedic = new UpgradeDefinition('MdMd', 'Medic Medic', 5);
upgAlliesMedicMedic.prevalidate = upgAlliesImprovedCommand.prevalidate;
upgAlliesMedicMedic.validate = upgAlliesImprovedCommand.validate;
var upgAlliesMrFixIt = new UpgradeDefinition('MrFx', 'Mr. Fixit', 5);
upgAlliesMrFixIt.prevalidate = upgAlliesImprovedCommand.prevalidate;
upgAlliesMrFixIt.validate = upgAlliesImprovedCommand.validate;
var upgAlliesCloseAirSupport = new UpgradeDefinition('ClsA', 'Close Air Support', 10);

var upgAlliesAirSupremacy = new UpgradeDefinition('ArSp', 'Air Supremacy', 10);
upgAlliesAirSupremacy.supports = [
    vhAlliesP48CDivingDotty, vhAlliesP48XBellowingBertie
];
upgAlliesAirSupremacy.validate = function (platoon)
{
    //  Limit once / force
    var numAirSupremacy = 0;
    for (var p in Force.platoons)
    {
        if (Force.platoons[p].upgrade == upgAlliesAirSupremacy)
        {
            if (++numAirSupremacy > 1)
            {
                ForceError("The 'Air Supremacy' Platoon Upgrade may only be taken once.");
                return false;
            }
            if (Force.platoons[p] == platoon)
                break;
        }
    }

    var numSections = 0;
    for (var s in platoon.sections) {
        if (platoon.sections[s]) ++numSections;
    }

    if (numSections < 4)
    {
        ForceError("The 'Air Supremacy' requires 4 sections be taken.");
        return false;
    }

    return true;
};

var upgAlliesDefenses = new UpgradeDefinition('ADfn', 'Defenses', 5);


var upgUnitAlliesSockAndTar     = new UpgradeDefinition('SkTr', 'Sock and Tar', 3);
var upgUnitAlliesM9DBazookas    = new UpgradeDefinition('M9DB', 'M9-D Bazookas', 1);
var upgUnitAlliesZombieHunters  = new UpgradeDefinition('ZmbH', 'Zombie Hunters', 2);
var upgUnitAlliesChaffDispersingUGLs = new UpgradeDefinition('ChfU', 'Chaff Dispersing UGLs', 2);

//Define command squad additional support units
sqAlliesRangerCommand.supports = [ null, sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam, sqAlliesFrenchForeignLegionKillSquad, sqAlliesUSMCHeavyMachineGunTeam, vhAlliesM1MobileHQ, sqAlliesUSMCAssault, sqAlliesUSMCObserver, sqAlliesSteelMarines ];
sqAlliesHeavyRangerCommand.supports = [ null, sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam, sqAlliesFrenchForeignLegionKillSquad, sqAlliesUSMCHeavyMachineGunTeam, vhAlliesM1MobileHQ, sqAlliesUSMCAssault, sqAlliesUSMCObserver, sqAlliesSteelMarines ];
sqAlliesRangerAttack.supports = [ null, sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam, sqAlliesFrenchForeignLegionKillSquad, sqAlliesUSMCHeavyMachineGunTeam, vhAlliesM1MobileHQ, sqAlliesUSMCAssault, sqAlliesUSMCObserver, sqAlliesSteelMarines ];


//Allied Combat Platoon Definition
var alliesCombatPlatoon = new PlatoonDefinition('ACmb', 'Combat Platoon');
alliesCombatPlatoon.sections = [
    [ sqAlliesRangerCommand, hBazookaJoe ].concat(hAlliesCanLeadAnyPlatoon),
    [ sqAlliesCombatRanger, sqAlliesAssaultRanger, sqAlliesRangerWeapon, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesHeavyRangerAttack, sqAlliesUSMCDemo ],
    [ null, sqAlliesCombatRanger, sqAlliesAssaultRanger, sqAlliesReconRanger, sqAlliesHeavyRangerAssault, sqAlliesSteelMarines, sqAlliesFrenchForeignLegionKillSquad, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesUSMCDemo ].concat(anyAllied_2ndSectionUnits),
    [ null, sqAlliesCombatRanger, sqAlliesRangerWeapon, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesHeavyRangerTankHunter, sqAlliesUSMCDemo ].concat(anyAllied_3rdSectionUnits),
    [ null, sqAlliesAssaultRanger, sqAlliesReconRanger, sqAlliesRangerWeapon, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad, sqAlliesUSMCDemo ].concat(anyAllied_4thSectionUnits)
];
alliesCombatPlatoon.supports = [
    sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam,
    sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam,
    sqAlliesUSMCFireSquad, sqAlliesUSMCDemo, sqAlliesUSMCForceRecon, sqAlliesUSMCAntiTank,
    vhAlliesM1MobileHQ,
    vhAlliesM3DCobra, vhAlliesM3ERattler,
    vhAlliesM5ASixShooter, vhAlliesM5BBulldog,
    vhAlliesM7LongTomII,
    vhAlliesM9Skysweeper,
    vhAlliesP48CDivingDotty, vhAlliesP48XBellowingBertie,
    vhStrongpoint, vhBunker, vhAlliesM47FieldPhaserStrongpoint
].concat(vhAlliesAnyM1LAW, vhAlliesAnyM2MCW, vhAlliesAnyM6HAW, anyAllied_SupportSectionUnits);

//Allied Elite Platoon Definition
var alliesElitePlatoon = new PlatoonDefinition('AElt', 'Elite Platoon');
alliesElitePlatoon.sections = [
    [ sqAlliesHeavyRangerCommand, hRhino, hOzz117 ].concat(hAlliesCanLeadAnyPlatoon),
    [ sqAlliesHeavyRangerAssault, sqAlliesSteelMarines, sqAlliesHeavyRangerAttack, sqAlliesHeavyRangerTankHunter, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad, sqAlliesFrenchForeignLegionKillSquad ],
    [ null, sqAlliesHeavyRangerAttack, sqAlliesHeavyRangerTankHunter, sqAlliesReconRanger, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad ].concat(anyAllied_2ndSectionUnits),
    [ null, sqAlliesHeavyRangerAttack, sqAlliesCombatRanger, sqAlliesRangerWeapon, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesUSMCDemo ].concat(anyAllied_3rdSectionUnits),
    [ null, sqAlliesHeavyRangerTankHunter, sqAlliesCombatRanger, sqAlliesRangerWeapon, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesReconRanger, sqAlliesUSMCDemo ].concat(anyAllied_4thSectionUnits)
];
alliesElitePlatoon.supports = [
    sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam,
    sqAlliesUSMCFireSquad, sqAlliesUSMCDemo, sqAlliesUSMCForceRecon, sqAlliesUSMCAntiTank,
    sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam,
    vhAlliesM1MobileHQ,
    vhAlliesM2AMickey, vhAlliesM2A2MickeyLight, vhAlliesM2BHotDog, vhAlliesM2CPounder, vhAlliesM2C2PounderLight,
    vhAlliesM5ASixShooter, vhAlliesM5BBulldog,
    vhStrongpoint, vhBunker, vhAlliesM47FieldPhaserStrongpoint
].concat(vhAlliesAnyM1LAW, anyAllied_SupportSectionUnits);

//Allied Assault Platoon Definition
var alliesAssaultPlatoon = new PlatoonDefinition('AAsl', 'Assault Platoon');
alliesAssaultPlatoon.sections = [
    [ sqAlliesRangerAttack, hJohnnyOneEye ].concat(hAlliesCanLeadAnyPlatoon),
    [ sqAlliesAssaultRanger, sqAlliesReconRanger, sqAlliesHeavyRangerAssault, sqAlliesSteelMarines, sqAlliesFrenchForeignLegionKillSquad ],
    [ null, sqAlliesAssaultRanger, sqAlliesReconRanger, sqAlliesHeavyRangerAttack ].concat(anyAllied_2ndSectionUnits),
    [ null, sqAlliesCombatRanger, sqAlliesAssaultRanger, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesUSMCDemo ].concat(anyAllied_3rdSectionUnits),
    [ null, sqAlliesReconRanger, sqAlliesRangerWeapon, sqAlliesHeavyRangerTankHunter, sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam, sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCFireSquad, sqAlliesUSMCAntiTank, sqAlliesUSMCForceRecon, sqAlliesUSMCDemo ].concat(anyAllied_4thSectionUnits)
];
alliesAssaultPlatoon.supports = [
    sqAlliesRangerObserverTeam, sqAlliesRangerSniperTeam,
    sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam,
    sqAlliesUSMCFireSquad, sqAlliesUSMCDemo, sqAlliesUSMCForceRecon, sqAlliesUSMCAntiTank,
    vhAlliesM1MobileHQ,
    vhAlliesM5ASixShooter, vhAlliesM5BBulldog,
    vhAlliesM7LongTomII,
    vhAlliesM9Skysweeper,
    vhAlliesP48CDivingDotty, vhAlliesP48XBellowingBertie,
    vhStrongpoint, vhBunker, vhAlliesM47FieldPhaserStrongpoint
].concat(vhAlliesAnyM1LAW, vhAlliesAnyM6HAW, anyAllied_SupportSectionUnits);

//Allied Assault Platoon Definition
var alliesUSMCExpeditionaryPlatoon = new PlatoonDefinition('AMCEP', 'USMC Expeditionary Platoon');
alliesUSMCExpeditionaryPlatoon.sections = [
    [ sqAlliesUSMCCommand ].concat(hAlliesCanLeadAnyPlatoon),
    [ sqAlliesUSMCFireSquad, sqAlliesUSMCRifle, sqAlliesUSMCDemo, sqAlliesUSMCForceRecon ],
    [ null, sqAlliesUSMCFireSquad, sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCForceRecon ].concat(anyAllied_2ndSectionUnits),
    [ null, sqAlliesUSMCFireSquad, sqAlliesSteelMarines, sqAlliesUSMCAntiTank, sqAlliesUSMCAssault ].concat(anyAllied_3rdSectionUnits, vhAlliesAnyM1LAW),
    [ null, sqAlliesSteelMarines, sqAlliesUSMCHeavyMortarTeam, sqAlliesUSMCAssault, sqAlliesUSMCAntiTank, sqAlliesUSMCDemo ].concat(anyAllied_4thSectionUnits)
];
alliesUSMCExpeditionaryPlatoon.supports = [
	sqAlliesUSMCObserver, sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam,
	vhAlliesM2GBarkingDogLight, vhAlliesM2AMickey, vhAlliesM2A2MickeyLight, vhAlliesM2A2MickeyLight
].concat(vhAlliesAnyM1LAW, vhAlliesAnyM6HAW, vhAlliesAnyM7HAW, vhAlliesAnyP48Aircraft, anyAllied_SupportSectionUnits);

//Special Platoons
//Allied Paratrooper Platoon Definition
var alliesParatroopPlatoon = new PlatoonDefinition('AJks', 'Action Jackson - British Paratroops');
alliesParatroopPlatoon.sections = [
    [ hActionJackson ],
    [ sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad ],
    [ null, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad ],
    [ null, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad ],
    [ null, sqAlliesBritishParatroops, sqAlliesBritishCommandosKillSquad ]
];
alliesParatroopPlatoon.upgradesAllowed = false;

//Allied Armoured Walker Platoon Definition
var alliesArmouredWalkerPlatoon = new PlatoonDefinition('AAWP', 'Armoured Walker Platoon');
alliesArmouredWalkerPlatoon.sections = [
    [ sqAlliesRangerCommand, sqAlliesHeavyRangerCommand, hBazookaJoe, hLieutenantColonelJosephBrown ],
    [].concat(vhAlliesAnyM1LAW, vhAlliesAnyM2MCW, vhAlliesAnyM3MCW),
    [null].concat(vhAlliesAnyM1LAW, vhAlliesAnyM2MCW, vhAlliesAnyM3MCW, vhAlliesAnyM5HDW),
    [null, vhAlliesM7Devestator].concat(vhAlliesAnyM1LAW, vhAlliesAnyM2MCW, vhAlliesAnyM3MCW, vhAlliesAnyM5HDW, vhAlliesAnyM6HAW),
    [null, vhAlliesM7Devestator].concat(vhAlliesAnyM2MCW, vhAlliesAnyM3MCW, vhAlliesAnyM5HDW, vhAlliesAnyM6HAW)
];
alliesArmouredWalkerPlatoon.supports = [
    sqAlliesReconRanger, sqAlliesUSMCHeavyMachineGunTeam,
    sqAlliesHeavyRangerAttack, sqAlliesHeavyRangerTankHunter, sqAlliesBritishParatroops,
    sqAlliesRangerObserverTeam,
    vhAlliesM7LongTomII,
    vhAlliesM9Skysweeper,
    vhAlliesP48CDivingDotty, vhAlliesP48XBellowingBertie,
    vhStrongpoint, vhBunker, vhAlliesM47FieldPhaserStrongpoint
];
alliesArmouredWalkerPlatoon.additionalSupports = {
    'Transport': [ null, vhAlliesM1MobileHQ, vhAlliesM2AMickey, vhAlliesM2A2MickeyLight ],
    'Support': [ null, vhAlliesM9Skysweeper ]
};
alliesArmouredWalkerPlatoon.validate = function(platoon)
{
    //  Can only take an M2 Mickey as a bonus support if taking Bazooka Joe
    if (platoon.bonusSupports['additionalTransport'] &&
        (platoon.bonusSupports['additionalTransport'].definition == vhAlliesM2AMickey || platoon.bonusSupports['additionalTransport'].definition == vhAlliesM2A2MickeyLight ) &&
        platoon.sections[0] != hBazookaJoe)
    {
        ForceError("Armoured Walker Platoon may only take an M2-A Mickey additional transport if led by Bazooka Joe.");
        return false;
    }
    return true;
};

//Allied USMC Weapon Support Platoon Definition
var alliesUSMCWeaponSupportPlatoon = new PlatoonDefinition('AMCWP', 'USMC Weapon Support Platoon');
alliesUSMCWeaponSupportPlatoon.sections = [
    [ sqAlliesUSMCCommand, hCrazyJimmy ],
    [ sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam, vhAlliesM7LongTomII ],
    [ null, sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam, vhAlliesM7LongTomII ],
    [ null, sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam, vhAlliesM7LongTomII, sqAlliesUSMCForceRecon],
    [ null, sqAlliesUSMCHeavyMachineGunTeam, sqAlliesUSMCHeavyMortarTeam, vhAlliesM9Skysweeper ]
];
alliesUSMCWeaponSupportPlatoon.supports = [
	sqAlliesUSMCForceRecon, sqAlliesUSMCObserver
];
alliesUSMCWeaponSupportPlatoon.additionalSupports = {
	    'Support': [ null, sqAlliesUSMCForceRecon, sqAlliesUSMCObserver, vhAlliesM1MobileHQ ],
	};
alliesUSMCWeaponSupportPlatoon.validate = function(platoon)
{
    if (platoon.bonusSupports['additionalSupport'] && platoon.sections[0].definition != sqAlliesUSMCCommand)
    {
        ForceError("USMC Weapon Support Platoon may only take an additional support choice if led by 'Mustangs'.");
        return false;
    }
    return true;
};

//Allied Faction Defintion
var alliesFaction = new FactionDefinition(Faction.Allies, 'Allies');
alliesFaction.upgrades = [ null, upgAlliesAddtlResources, upgAlliesAirDrop,
    upgAlliesImprovedCommand, upgAlliesMedicMedic, upgAlliesMrFixIt,
    upgAlliesAeroncaStrike, upgAlliesExtraHero, upgAlliesPreparatoryBarrage,
    upgAlliesCloseAirSupport, upgAlliesAirSupremacy, upgAlliesDefenses ];
alliesFaction.unitUpgrades = [ upgUnitAlliesSockAndTar, upgUnitAlliesM9DBazookas, upgUnitAlliesZombieHunters, upgUnitAlliesChaffDispersingUGLs ];

alliesFaction.platoons = [ alliesCombatPlatoon, alliesElitePlatoon, alliesAssaultPlatoon, alliesParatroopPlatoon, alliesArmouredWalkerPlatoon, alliesUSMCExpeditionaryPlatoon, alliesUSMCWeaponSupportPlatoon, mercCleaningPlatoon ];
alliesFaction.heroes = [ hBazookaJoe, hRhino, hActionJackson, hRosie, hOzz117, hThePriest, hJohnnyOneEye, hTheChef, hSergeantVictory, 
                         hLieutenantColonelJosephBrown, hMacheteMack, hQuietJack, hLittleMack, hCrazyJimmy].concat(hMercAnyHeroes);
